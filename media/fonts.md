# Fonts


## Windows Terminal (CP437)

- aka `OEM PC-8`
- files: `dosapp.fon`, `vgaoem.fon`
- find alternatives:
    - <https://www.dafont.com/search.php?q=437> => *Moder DOS 437*, *Perfect DOS VGA 437*
- download:
    - <http://www.softpedia.com/get/Others/Font-Utils/Terminal-Font.shtml#download>


## Fixed width fonts

### Linux

- Ubuntu Mono
- Droid Sans Mono
- DejaVu Sans Mono
- Liberation Mono
- Inconsolata (`sudo apt install ttf-inconsolata`)
- Terminus (`sudo apt install xfonts-terminus`)


## Installation

- Windows: copy to `c:\windows\fonts`
- Linux: copy to `~/.fonts` or `/usr/share/fonts` and run `fc-cache -f -v`

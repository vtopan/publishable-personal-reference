# Links


## Online Tools

- code search:
    - <http://opensearch.krugle.org/document/search/>


## Dev

- generic
    - [dependency_spy](https://github.com/rtfpessoa/dependency_spy) - Finds known vulnerabilities in your dependencies
        using yavdb as the source agregator of vulnerabilities.

### vim

- sample `init.vim`: <https://gitlab.com/snippets/25691>
- plugins
    - <https://vimawesome.com/> Awesome Vim plugins from across the Universe


### C

- <https://github.com/uhub/awesome-c> A curated list of awesome C frameworks, libraries and software.
- <https://github.com/vurtun/nuklear> A single-header ANSI C gui library


#### Secure Coding in C

- [OWASP Cheatsheets](https://www.owasp.org/index.php/Category:Cheatsheets)
- blog: [Integer Undefined Behaviors in Open Source Crypto Libraries](https://blog.regehr.org/archives/1054)
- Fedora reference: [Defensive Coding - Chapter 1. The C Programming Language]( https://docs-old.fedoraproject.org/en-US/Fedora_Security_Team/1/html/Defensive_Coding/chap-Defensive_Coding-C.html)
- D.Wheeler - [Secure Programming HOWTO - Creating Secure Software](https://www.dwheeler.com/secure-programs/) (book)
- [Compiler Explorer](https://gcc.godbolt.org/) (online live C compilers)


##### Courses

- **[Offensive Computer Security Spring](https://www.cs.fsu.edu/~redwood/OffensiveComputerSecurity/lectures.html)** @ *Florida (2014) State University*:
    - Lect. 2: [Secure C Coding 101](https://docs.google.com/presentation/d/1nIKo4LLuuUsU-BN23m2zFQgdi7RCmK-SwFbLkssIuwc/edit?usp=sharing)
    - Lect. 3: [Secure C Coding 102](https://docs.google.com/presentation/d/1EhwXYr2Ffe3VQlkH-PcJ3FXN4xCVvYohC5h6NL0sKw0/edit?usp=sharing)
    - Lect. 4: [Code Auditing](https://docs.google.com/presentation/d/1mi8cCN6EU7P-eZrA9ngGT1Bxu9I74-A9nHt4OE2oupQ/edit?usp=sharing)
- **opensecuritytraining.info**:
    - [Introduction to Secure Coding](http://www.opensecuritytraining.info/IntroSecureCoding.html) ([pdf](http://www.opensecuritytraining.info/IntroSecureCoding_files/intro_secure_coding_20141217.pdf))
    - [Secure Code Review](http://www.opensecuritytraining.info/SecureCodeReview.html) ([pdf](http://www.opensecuritytraining.info/SecureCodeReview_files/secure_code_review_20141217.pdf))
- [Secure Programming in C](http://web.mit.edu/6.s096/www/lecture/lecture03/secure-C.pdf) @ MIT (2014)
- **[INFSCI 2620 Developing Secure Systems](http://www.sis.pitt.edu/jjoshi/courses/IS2620/Spring07/Lectures.html)** @ U.Pittsburgh (2007)
    - Lect. 3: [String Vulnerabilities](http://www.sis.pitt.edu/jjoshi/courses/IS2620/Spring07/Lecture3.pdf)
    - Lect. 4: [Race conditions](http://www.sis.pitt.edu/jjoshi/courses/IS2620/Spring07/Lecture4.pdf)
    - Lect. 5: [Dynamic Memory Management](http://www.sis.pitt.edu/jjoshi/courses/IS2620/Spring07/Lecture5.pdf)
    - Lect. 6: [Integer Security](http://www.sis.pitt.edu/jjoshi/courses/IS2620/Spring07/Lecture6.pdf)
    - Lect. 7: [Pointer Subterfuge](http://www.sis.pitt.edu/jjoshi/courses/IS2620/Spring07/Lecture7.pdf)


##### C Secure Coding Standards

- [CERT - C Programming Language Secure Coding Standard](http://www.open-std.org/jtc1/sc22/wg14/www/docs/n1255.pdf) (N1255 @ 2007)
- ISO/IEC *C Secure Coding Rules* (ISO/IEC TS 17961):
    - [N1624 @ Jun. 2012](http://www.open-std.org/jtc1/sc22/wg14/www/docs/n1624.pdf)
    - [N1669 @ Dec. 2012](http://www.open-std.org/jtc1/sc22/wg14/www/docs/n1669.pdf)
- CERT wiki:
    - [SEI CERT C Coding Standard](https://wiki.sei.cmu.edu/confluence/display/c)
    - [SEI CERT C++ Coding Standard](https://wiki.sei.cmu.edu/confluence/pages/viewpage.action?pageId=88046682)

    
### Python

- <https://github.com/vinta/awesome-python> A curated list of awesome Python frameworks, libraries, software and
    resources
- web:
    - [Responder](https://python-responder.org/en/latest/) nweb service framework, written for human beings.


### nim
- <https://totallywearingpants.com/posts/nim-underdog/> nim - A Programming Language Underdog


### ML

- Tensorflow
    - <https://github.com/deepmind/trfl/> TRFL (pronounced "truffle") is a library built on top of TensorFlow that
        exposes several useful building blocks for implementing Reinforcement Learning agents.
- [Kalimdor](https://www.kalimdor.io/) - Machine Learning library for the web and Node (JS/Typescript).


## Security


### Crypto

- <https://tls.ulfheim.net/> The Illustrated TLS Connection - Every byte of a TLS connection explained and reproduced.


### Malware

- <https://github.com/rshipp/awesome-malware-analysis> A curated list of awesome malware analysis tools and resources.
- sample download:
    - <https://virusshare.com/>
    - <https://malshare.com/>
    - <http://www.virusign.com/>
    - <https://www.hybrid-analysis.com/>
    - <http://dasmalwerk.eu/>
    - <https://github.com/ytisf/theZoo>
    - <http://www.kernelmode.info/forum/viewforum.php?f=16>
    - probably dead: <https://malwr.com/>


### Reverse engineering

- [Kaitai](http://kaitai.io/) - declarative language describing file formats
    - has a [web-based IDE](https://github.com/koczkatamas/kaitai_struct_webide)
- [x64dbg](http://x64dbg.com/#start) - Olly-like open-source debuger for 32 & 64 bit PEs


### Papers

- [Decompiling the vulnerable function for MS08-067](www.phreedom.org/blog/2008/decompiling-ms08-067/), Alexander
    Sotirov
- [Hello MS08-067, My Old Friend!](https://labs.mwrinfosecurity.com/assets/BlogFiles/hello-ms08-067-my-old-friend.pdf),
    MWR Labs


### Pentesting

- <https://highon.coffee/blog/lfi-cheat-sheet/>


#### Data

- [ropshell](http://ropshell.com/) - ROP gadget repo


#### Tools

- [Burp](https://portswigger.net/burp/download.html)
- [CyberChef](https://gchq.github.io/CyberChef/) - simple, intuitive web app for analysing and decoding data
- [Fiddler](http://www.telerik.com/fiddler) - web proxy (pentesting,webdev)


#### Web

- <https://tools.kali.org/information-gathering/dotdotpwn> intelligent fuzzer to discover traversal directory
  vulnerabilities in software such as HTTP/FTP/TFTP servers, Web platforms such as CMSs, ERPs, Blogs, etc.
- <http://www.cgisecurity.com/lib/urlembeddedattacks.html> URL Encoded Attacks
- <https://www.gracefulsecurity.com/path-traversal-cheat-sheet-windows/> Path Traversal Cheat Sheet: Windows
- <https://www.gracefulsecurity.com/path-traversal-cheat-sheet-linux/> Path Traversal Cheat Sheet: Linux


#### Training

- [avatao](https://platform.avatao.com/discover/paths) - wargame-style pentesting challenges, some free (CrySyS Lab,
  Budapest, Hungary)
- [pentestit.ru](https://lab.pentestit.ru/) - 6-month free OSCP-style pentesting training networks (Moscow)
- [elearnsecurity](https://www.elearnsecurity.com/course/penetration_testing/) - $


#### Relevant blogs

- [j00ru](http://j00ru.vexillium.org/)
- [Project Zero (Google)](https://googleprojectzero.blogspot.ro/)
    - bugs [here](https://bugs.chromium.org/p/project-zero/issues/list?can=1&q=)
- [threatpost](https://threatpost.com/)
- Reddit: [r/netsec](https://www.reddit.com/r/netsec/), [r/hacking](https://www.reddit.com/r/hacking/)


## Browser


### Firefox plugins

- [Markdown Viewer Webext](https://addons.mozilla.org/en-US/firefox/addon/markdown-viewer-webext/)
    - Displays markdown documents beautified in your browser
- [Firebug](https://addons.mozilla.org/en-US/firefox/addon/firebug/) (pentesting,webdev)
- [Greasemonkey](https://addons.mozilla.org/en-US/firefox/addon/greasemonkey/) (pentesting,webdev,home)
- [Web Developer](https://addons.mozilla.org/en-US/firefox/addon/web-developer/) (pentesting,webdev)
- [Tamper Data](https://addons.mozilla.org/en-US/firefox/addon/tamper-data/) (pentesting,webdev)
- [Cookies Manager+](https://addons.mozilla.org/en-US/firefox/addon/cookies-manager-plus/) (pentesting,webdev,home)
- [SQL Inject Me](https://addons.mozilla.org/en-US/firefox/addon/sql-inject-me/) (pentesting,websec)
- [JS Deobfuscator](https://addons.mozilla.org/en-US/firefox/addon/javascript-deobfuscator/) (reveng,websec)
- [Wappalyzer](https://addons.mozilla.org/en-US/firefox/addon/wappalyzer/) - web app id (pentesting,websec)
- [FirePath](https://addons.mozilla.org/en-US/firefox/addon/firepath/) - XPath expression analyzer (websec,home)
- [Video DownloadHelper](https://addons.mozilla.org/en-US/firefox/addon/video-downloadhelper/) (home,video)
- [Flash Video Downloader - YouTube HD Download](https://addons.mozilla.org/en-US/firefox/addon/flash-video-downloader/)
    (home,video)
- [UnPlug](https://addons.mozilla.org/en-US/firefox/addon/unplug/) (home,video)
- [UnMHT](https://addons.mozilla.org/en-US/firefox/addon/unmht/) (home,file-format)
- [TinEye Reverse Image Search](https://addons.mozilla.org/en-US/firefox/addon/tineye-reverse-image-search/)
    (home,search,images)
- [Search By Image (by Google)](https://addons.mozilla.org/en-US/firefox/addon/search-by-image-by-google/)
    (home,search,images)
- [Thumbs](https://addons.mozilla.org/en-US/firefox/addon/thumbs/) (home,images)
- [NoScript](https://addons.mozilla.org/en-US/firefox/addon/noscript/) (home,js,pentesting)
- [Greasemonkey](https://addons.mozilla.org/en-US/firefox/addon/greasemonkey/)
- [Ghostery](https://addons.mozilla.org/en-US/firefox/addon/ghostery/) (home,js,privacy)
- [Adblock Plus](https://addons.mozilla.org/en-US/firefox/addon/adblock-plus/) (home,js,privacy,speed)


### Themes

- Stylus:
    - [New DuckDuckGO - Dark Theme](https://userstyles.org/styles/101431/new-duckduckgo-dark-theme)
    - [Dark and Clean Pastebin](https://userstyles.org/styles/132240/dark-and-clean-pastebin)
    - [Userstyles Dark | aperopia](https://userstyles.org/styles/105609/userstyles-dark-aperopia)
    - [Youtube Umbra](https://userstyles.org/styles/130610/youtube-umbra)


## Music

- [Coldplay - Viva La Vida](https://www.youtube.com/watch?v=dvgZkm1xWPE)


## Books


### Indexes

- [Library Genesis](https://libgen.io/)
- <http://b-ok.xyz/>


### Lists

- [Time's List of the 100 Best Novels](https://en.wikipedia.org/wiki/Time%27s_List_of_the_100_Best_Novels)
- [The Big Read](https://en.wikipedia.org/wiki/The_Big_Read)
- [Le Monde's 100 Books of the Century](https://en.wikipedia.org/wiki/Le_Monde%27s_100_Books_of_the_Century)
- [Penguin Red Classics](https://en.wikipedia.org/wiki/Penguin_Red_Classics)
- [Penguin Essentials](https://en.wikipedia.org/wiki/Penguin_Essentials)


## Misc

- disposable emails: [trashmail](http://trashmail.com/), [mailinator](http://mailinator.com/)
- click-to-play musical notes: <https://onlinesequencer.net/>


### File formats

- [mermaid](https://mermaidjs.github.io/) Generation of diagrams and flowcharts from text in a similar manner as
  markdown.


### Linux tools

- [entr](http://entrproject.org/) Run arbitrary commands when files change

# File formats

- excellent visuals from Corkami for many file formats: <https://github.com/corkami/pics/tree/master/binary/>
- [PE](pe.md), [PDF](pdf.md)


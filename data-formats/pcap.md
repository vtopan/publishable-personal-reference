# PCAP

- online repositories:
    - [Netresec - Publicly available PCAP files](https://www.netresec.com/?page=PcapFiles)
    - [Wireshark - SampleCaptures](https://wiki.wireshark.org/SampleCaptures)
- online analysis:
    - <https://www.cloudshark.org/>
        - find captures: e.g. `https://www.google.ro/search?q=site%3Awww.cloudshark.org/captures%2F+modbus`
    - <http://pcapr.net/home>
        - needs free account
        - search: http://www.pcapr.net/browse?q=profinet

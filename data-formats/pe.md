# PE file format

- excellent graphics of the format: Corkami <https://raw.githubusercontent.com/corkami/pics/master/binary/pe101/pe101.pdf>
- PE quirks: <https://github.com/corkami/docs/blob/master/PE/PE.md>
- abusing the header format - samples from Corkami: `https://github.com/corkami/pocs/tree/master/PE`

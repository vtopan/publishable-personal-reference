# PDF Processing


## Use pdftk

- remove last page: `pdftk file.pdf cat 1-r2 output out.pdf`
    - separate multiple ranges with spaces, rN means "last N-1 page(s)"
- concatenate PDFs: `pdftk file1.pdf file2.pdf ... cat output out.pdf`
- split each page as a document: `pdftk file.pdf burst`

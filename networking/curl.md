# curl


## Args

- `-U user:password`: proxy user/password
- `-k`: ignore server certificate
- `-d @filename`: get (ASCII) POST data from file
- `-H "Header: Value"`: add HTTP header
- `-x PROXYURL`: proxy (prefix with protocol, e.g. "socks4://...")
- `-u user:password`: HTTP AUTH
- `-T filename`: upload file (HTTP PUT or FTP)
- `-X POST`: HTTP METHOD
- `-F field=value`: post FORM data (to upload a file: `-F file=@/path/to/file`)


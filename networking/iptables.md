# iptables


## Misc usage

- redirect IP: `iptables -t nat -A OUTPUT -d <from-IP> -j DNAT --to-destination <to-IP>`


## Examples

- rate limiting:

```
$IPT -I INPUT -p tcp --dport ${port} -i ${if} -m state --state NEW -m recent --set
$IPT -I INPUT -p tcp --dport ${port} -i ${if} -m state --state NEW -m recent --update --seconds 60 --hitcount 5 -j DROP
```

or

```
$IPT -A INPUT -i ${if} -m state --state NEW,ESTABLISHED,RELATED -p tcp --dport 22 -m limit --limit 5/minute --limit-burst 5 -j ACCEPT
```

or 

```
$IPT -A INPUT  -i ${if} -p tcp --dport ${port} -m state --state NEW -m limit --limit 3/min --limit-burst 3 -j ACCEPT
$IPT -A INPUT  -i ${if} -p tcp --dport ${port} -m state --state ESTABLISHED -j ACCEPT
$IPT -A OUTPUT -o ${if} -p tcp --sport ${port} -m state --state ESTABLISHED -j ACCEPT
```

# ifconfig


## Cloning a NIC (adding an IP address)

- add *10.1.1.1* to *eth0*: `ifconfig eth0:0 10.1.1.1 netmask 255.255.255.255 up`

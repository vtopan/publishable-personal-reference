# wget


## Mirroring

- mirror site: `wget -mxkK -e robots=off http://the-site-you-want-to-mirror.com -U "$UA"`


## Useful params

```
option                  effect
----------------------- ---------
-m                      mirror (-r -N -l inf --no-remove-listing)
-l depth                recursive depth
-k                      convert links
-K                      keep original files as .orig (with -k)
-x                      create subfolders
-r                      recursive
-p                      page requisites
-o log                  log messages to ``log`` instead of ``stderr``
-c                      continue download
-T sec                  set all timeouts to ``sec``
-U ua                   user agent
-e cmd                  execute .wgetrc command
--user=... / --password=...
--load-cookies file
--save-cookies file
--keep-session-cookies  normally skipped
--header=line
--proxy-user=...
--proxy-password=...
--referer=url
--save-headers          include headers in result
--post-data=...
--post-file=filename
--no-check-certificate
```

- variables used: `$http_proxy`, `$https_proxy`, `$ftp_proxy`

# Miscellaneous Networking

- find your public IP: `curl http://icanhazip.com`


## Linux

- Killing TIME_WAIT connections:
    - `echo 1 > /proc/sys/net/ipv4/tcp_fin_timeout`
    - or `net.ipv4.tcp_fin_timeout = 1` in *sysctl.conf*

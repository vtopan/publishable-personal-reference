# socat


## Examples

```
socat TCP-LISTEN:80,fork TCP:1.2.3.4:80
socat TCP-LISTEN:3260,reuseaddr,fork,su=nobody TCP:127.0.0.1:32600
```
# tcpdump


## Parameters

- `-i <if>`: network interface to listen on (`any`: all; default: the lowest numbered, up interface)
- `-s ###`: max. bytes to capture from each packet (0 = default = 262144 bytes); set as low as possible
- `-T <type>`: treat packets as `<type>` (can be: *resp*, *radius*, *rpc*, *rtp*, *rtcp*, *snmp*, *tftp* etc.)
- `-t`: don't print timestamp
- `-w <file>`: write dump to file as well
- `-v`: more verbose output (TTL, ident, total len, options etc.); `-vv`: even more (e.g. fully decode *SMB*)


## Examples

- DHCP packets: `-s1500 '(port 67 or port 68)'` (or `port bootps`)
- SNMP packets: `-T snmp "(port 161)"`

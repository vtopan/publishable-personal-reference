# RDP


## xfreerdp

- e.g.: `xfreerdp /u:user /p:pass /v:10.1.2.3`
- resolution: `/size:1400x900`
- clipboard: `+clipboard`
- ClearType (smooth fonts): `+fonts`
- ignore remote host cert: `+cert-ignore`

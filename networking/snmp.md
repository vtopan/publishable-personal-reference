# SNMP

- "SNMP is an Internet-standard protocol for collecting and organizing information about managed devices on IP networks
    and for modifying that information to change device behavior"
- network protocol over UDP (port 161)
- queries the MIB (management information database) stored e.g. in switches (obtained via LLDP/CDP)
- three versions: "1" (obsolete), "2c" (uses a "community string" as an authentication mechanism) and "3" (does proper
    authentication and optional traffic encryption)
- to get information about an OID search online for "oid-info <the-oid>", e.g. "oid-info 1.3.6.1.2.1.17.7.1.2.2" or
    "oid-info dot1qTpFdbTable" (this should lead to a page on <http://oid-info.com> which describes the OID);
    alternatively, open the direct URL `http://oid-info.com/get/1.[...]`
- SNMP tools on Ubuntu: `apt install snmp snmp-mibs-downloader` (the `...-downloader` retrieves MIB tables from the
    Internet)
- minimal `snmpwalk` call template: `snmpwalk -v2c -c <commstring> <host-address> [<oid>]`


## Relevant OIDs

- `system` (`1.3.6.1.2.1.1`) in `RFC 1213`:
    - contains generic system information
    - fields: `sysDescr(1)` - textual description of the entity, `sysObjectID(2)` - vendor OID describing the entity,
        `sysUpTime(3)` - uptime in hundreds of seconds, `sysContact(4)` - contact person, `sysName(5)` - name (FQDN) of
        the node, `sysLocation(6)` - physical location of the node, `sysServices(7)` - bitmap of each ISO/OSI stack
        layer on which this node operates (e.g. 6 = 2 + 4 = switch + router), `sysORLastChange(8)`, `sysORTable(9)`

- `ifTable` (`1.3.6.1.2.1.2.2`) in `RFC 1213`:
    - describes the network interfaces present on the node
    - `ifNumber` (`1.3.6.1.2.1.2.1`): number of interfaces
    - `ifTable`(`1.3.6.1.2.1.2.2`): interface table
        - fields: `ifEntry(1)`: {`ifIndex(1)`, `ifDescr(2)`, `ifType(3)` - see
            [here](https://www.iana.org/assignments/smi-numbers/smi-numbers.xhtml#smi-numbers-5) for a list, `ifMtu(4)`,
            `ifPhysAddress(6)` - MAC address, `ifOperStatus(8)` - 1 = up / 2 = down / 3 = testing, `ifLastChange(9)`,
            `ifInOctets(10)`, `ifOutOctets(16)` etc.
        - partial example, e.g. interface 581:

            ```
            iso.3.6.1.2.1.2.2.1.1.581 = INTEGER: 581
            iso.3.6.1.2.1.2.2.1.2.581 = STRING: "vlan.2"
            iso.3.6.1.2.1.2.2.1.3.581 = INTEGER: 53
            iso.3.6.1.2.1.2.2.1.4.581 = INTEGER: 1514
            iso.3.6.1.2.1.2.2.1.6.581 = Hex-STRING: 12 23 34 12 34 56
            iso.3.6.1.2.1.2.2.1.8.581 = INTEGER: 1
            iso.3.6.1.2.1.2.2.1.9.581 = Timeticks: (1234) 0:00:12.34
            iso.3.6.1.2.1.2.2.1.10.581 = Counter32: 12346341
            iso.3.6.1.2.1.2.2.1.16.581 = Counter32: 4322931
            ```

            - interface 581 is up (.8 = 1), name "vlan.2", type 53 ("proprietary virtual/internal interface"),
                MTU 1514, MAC address "12:23:34:12:34:56", in this state since node uptime was 0:00:12.34,
                received 12946343 bytes, sent 4362921

- `dot1qTpFdbTable` (`1.3.6.1.2.1.17.7.1.2.2`) in `Q-BRIDGE-MIB`:
    - maps MAC addresses to the (bridge) ports through which they must be routed
    - description: "A table that contains information about unicast entries for which the device has forwarding and/or
        filtering information. This information is used by the transparent bridging function in determining how to
        propagate a received frame."
    - fields: `dot1qTpFdbEntry(1)`: {`dot1qTpFdbAddress(1)`, `dot1qTpFdbPort(2)`, `dot1qTpFdbStatus(3)`}
       - status: other(1), invalid(2), learned(3), self(4), mgmt(5)
    - example:

        ```
        iso.3.6.1.2.1.17.7.1.2.2.1.2.3.0.12.13.14.1.1 = INTEGER: 519
        iso.3.6.1.2.1.17.7.1.2.2.1.3.3.0.12.13.14.1.1 = INTEGER: 3
        ```

        - .1 (the address) is missing from the entry, but it's not essential
        - means that MAC 0.12.13.14.1.1 ('00:0C:0D:0E:01:01') is to be routed via port 519 via a learned (3) route

    - see also: `dot1dTpFdbTable` (`1.3.6.1.2.1.17.4.3`) - same info, also has age

- `ipAddrTable` (`1.3.6.1.2.1.4.20`) in `SNMPv2-MIB`:
    - maps IP addresses (and netmasks) to interface indexes
    - description: "Table of addressing information relevant to this entity's Internet Protocol (IP) addresses."
    - fields: `ipAddrEntry(1)`: {`ipAdEntAddr(1)`, `ipAdEntIfIndex(2)`, `ipAdEntNetMask(3)`, `ipAdEntBcastAddr(4)`,
        `ipAdEntReasmMaxSize(5)`}
    - example:

        ```
        iso.3.6.1.2.1.4.20.1.1.10.1.2.3 = IpAddress: 10.1.2.3
        iso.3.6.1.2.1.4.20.1.2.10.1.2.3 = INTEGER: 600
        iso.3.6.1.2.1.4.20.1.3.10.1.2.3 = IpAddress: 255.255.255.0
        iso.3.6.1.2.1.4.20.1.4.10.1.2.3 = INTEGER: 1
        iso.3.6.1.2.1.4.20.1.5.10.1.2.3 = INTEGER: 65535
        ```

        - => IP 10.1.2.3 (mask: 255.255.255.0) belongs to the interface with index `600`

- `ipRouteTable` (`1.3.6.1.2.1.2.2.1.3`) in `RFC 1213`:
    - "This entity's IP Routing table."
    - fields: `ipRouteEntry(1)`: {`ipRouteDest(1)`, `ipRouteIfIndex(2)`, `ipRouteNextHop(7)`, `ipRouteType(8)`,
        `ipRouteProto(9)`, `ipRouteAge(10)` ...}

- `ipNetToMediaTable` (`1.3.6.1.2.1.4.22`) in `RFC 1213`:
    - "The IP Address Translation table used for mapping from IP addresses to physical addresses."
    - fields: `ipNetToMediaEntry(1)`: {`ipNetToMediaIfIndex(1)`, `ipNetToMediaPhysAddress(2)`,
        `ipNetToMediaNetAddress(3)`, `ipNetToMediaType(4)`}

- `dot1dBasePortTable` (`1.3.6.1.2.1.17.1.4`) - maps ports to interfaces
- `ipAddrTable` (`1.3.6.1.2.1.4.20`) - maps IP addresses to interfaces
- `dot1qVlanStaticTable` (`1.3.6.1.2.1.17.7.1.4.3`) - static (pre-configured) VLAN information
- `tcpConnTable` (`1.3.6.1.2.1.6.13.1`) - this node's TCP connections
- `ifXEntry` (`1.3.6.1.2.1.31.1.1.1`) - additional information about interfaces
- Juniper: `1.3.6.1.4.1.2636.3.1.2`, `1.3.6.1.4.1.2636.3.1.3`
- LLDP neighbors:
    - <http://oid-info.com/get/1.0.8802.1.1.2.1.4.1.1>
    - <http://www.mibdepot.com/cgi-bin/getmib3.cgi?win=mib_a&i=1&n=LLDP-MIB&r=avaya&f=LLDP-MIB.mib&v=v2&t=tbl&o=lldpRemTable>
    - <https://stackoverflow.com/questions/15898109/find-neighbour-with-snmp-lldp>
- CDP neighbors:
    - <http://oid-info.com/get/1.3.6.1.4.1.9.9.23.1.2.1>
    - <https://www.oidview.com/mibs/9/CISCO-CDP-MIB.html>
    - <https://ipmsupport.solarwinds.com/mibs/CISCO-CDP-MIB/raw.aspx>
    - <https://sourceforge.net/p/netdisco/mibs/ci/master/tree/cisco/CISCO-TC.my>

    
## Vendor-specific tables

### VLANs

- `netgearVlanStaticId` (`1.3.6.1.4.1.4516.1.2.13.1.1.1`)
- `netgearVlanTaggedTable` (`1.3.6.1.4.1.4526.1.2.13.2.1`)  ("...4526.1.2.13.2.1.1.<port-id>.<vlan-id>")
- `ciscoVtpVlanState` (`1.3.6.1.4.1.9.9.46.1.3.1.1.2`)
- `ciscoVmVlan` (`1.3.6.1.4.1.9.9.68.1.2.2.1.2`)
- `extremeVlanStaticName` (`1.3.6.1.4.1.1916.1.2.1.2.1.2`)
- `extremeVlanTaggedType` (`1.3.6.1.4.1.1916.1.2.3.1.1.2`)
- `threecomVlanStaticName` (`1.3.6.1.4.1.43.10.1.14.1.2.1.2`)
- `threecomVlanTaggedType` (`1.3.6.1.4.1.43.10.1.14.4.1.1.2`)
- Juniper: <https://www.juniper.net/documentation/en_US/junos12.3/topics/reference/general/snmp-ex-vlans-retrieving.html>


### Misc

- `extremeSystemID` (`1.3.6.1.4.1.1916.1.1.1.18.0`)
- `apcSerialNumber` (`1.3.6.1.4.1.318.1.1.12.1.6.0`)


## Mapping a switch interface to the connected MAC (remote host)

- get interface index (`$ifindex`)
- list all VLANs on the switch (vmVlan = "1.3.6.1.4.1.9.9.68.1.2.2.1.2")
    - => `$vlan` = `VLAN[$ifindex]`
- map VLAN ports to MAC (dot1dTpFdbAddress = "1.3.6.1.2.1.17.4.3.1.1")
    - => `$vlan2mac`
- map $bridgePort to VLAN (dot1dTpFdbPort = "1.3.6.1.2.1.17.4.3.1.2")
    - inverse => `$port2vlan`
- map $ifindex to (bridge) port (dot1dBasePortIfIndex = "1.3.6.1.2.1.17.1.4.1.2")
    - inverse => `$index2port`
- `$bridgePort` = `$index2port[ifindex]`
- `$portVlan` = `$port2vlan[$bridgePort]`
- `$mac` = `$vlan2mac[$portVlan]`
- source: <https://stackoverflow.com/questions/5647294/perl-netsnmpinterfacesdetails-how-to-get-mac-address>


# Reference

- <https://community.rapid7.com/community/infosec/blog/2013/03/07/video-tutorial-introduction-to-pen-testing-simple-network-management-protocol-snmp>
- tcpdump/wireshark dumping: <https://communities.ca.com/thread/101923301>
- vendor-specific MAC interrogation: <https://crack3r.net/mediawiki/index.php/Snmp_mac2port>
- [Using SNMP to Find a Port Number from a MAC Address on a Catalyst Switch](http://www.cisco.com/c/en/us/support/docs/ip/simple-network-management-protocol-snmp/44800-mactoport44800.html#tpic2)
- learned MACs: <https://www.force10networks.com/CSPortal20/TechTips/0014_SNMP-HowtoGetDynamicMACCamEntries.aspx>
- vendor-specific VLAN info: <https://github.com/benroeder/HEN/blob/master/lib/auxiliary/oid.py>
- Juniper VLAN info: <https://www.juniper.net/documentation/en_US/junos12.3/topics/reference/general/snmp-ex-vlans-retrieving.html>

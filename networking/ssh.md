# ssh


## Arguments

- `-X` = `ForwardX11 yes`
- `-x` = `ForwardX11 no`
- `-T` = disable pseudo-tty allocation
- `-t` = force pseudo-tty allocation (`RequestTTY yes`) - fixes "sudo: no tty present and no askpass..."
- `-C` = `Compression yes`
- `-S` = read password from stdin


## Use ssh agent

- start the ssh-agent in the background: `eval "$(ssh-agent -s)"`
- add key to ssh agent: `ssh-add ~/.ssh/id_rsa`
- auto-start:

    - add to `.bashrc`:

        ```bash
        # start ssh-agent
        if [ ! -S ~/.ssh/ssh_auth_sock ]; then
            eval `ssh-agent`
            ln -sf "$SSH_AUTH_SOCK" ~/.ssh/ssh_auth_sock
        fi
        export SSH_AUTH_SOCK=~/.ssh/ssh_auth_sock
        ssh-add -l > /dev/null || ssh-add
        ```

    - add to `.bash_logout`:

        ```bash
        if [ -n "$SSH_AUTH_SOCK" ] ; then
            eval `/usr/bin/ssh-agent -k`
        fi
        ```


## Misc

- allocate pseudo tty (handle `sudo` over `ssh`): `ssh -t ...`
- generate key: ``ssh-keygen -t rsa -b 4096 -C "your_email@example.com"``
- check config before restarting: ``sshd -t``
- allow *scp* / *sftp* without enabling a shell: install `rssh`, set `/usr/bin/rssh` as shell and enable services in
    `/etc/rssh.conf`


## Troubleshooting


### byobu (Ubuntu/SSH) via Putty keeps scrolling

- problem: Ubuntu/SSH sends UTF8, Putty expects ISO-8859-1 by default
- fix: Settings -> Window -> Translation -> Received data assumed to be in which character set := UTF-8


### sudo over ssh fails: "sudo: no tty present and no askpass program specified"

- problem: sudo needs a TTY to ask for the password
- fix: pass `-t` to `ssh`


### Host key verification failed.

- programatically add host to known_hosts (when missing): `ssh-keyscan -H -t rsa <host> >> ~/.ssh/known_hosts`

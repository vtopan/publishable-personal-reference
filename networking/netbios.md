# NetBIOS

- get NetBIOS info on Windows: `nbtstat -n`
- lookup host on Linux: `nmblookup ...`
- via nmap:
    - find netbios machines: `nmap -sV -p 139,445 <network>`
    - get name: `nmap --script=nbstat -p U:137 <target>`

# Fiddler

## Reverse proxy

- `Tools > Fiddler Options > Allow remote clients to connect`
- `Rules > Customize Rules`, inside the OnBeforeRequest handler: 
    - `if (oSession.host.toLowerCase() == "webserver:8888") oSession.host = "webserver:80";`

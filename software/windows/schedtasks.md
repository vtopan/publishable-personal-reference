# Scheduled Tasks

- list: `schtasks`
    - full details: `/v`
    - output format: `/fo {csv|list|table}`
- delete: `schtasks /delete /tn ...`
    - e.g. `schtasks /delete /tn "\Microsoft\Windows\RemovalTools\MRT_ERROR_HB"`
- create: `schtasks create /TN <name> /TR <command>`
    - repeat: 
        - frequency: `/SC <frequency> /MO <modifier>`
            - `<frequency>`: `MINUTE, HOURLY, DAILY, WEEKLY, MONTHLY, ONCE, ONSTART, ONLOGON, ONIDLE, ONEVENT`
            - `<modifier>`:     
                - `MINUTE`: 1 - 1439 minutes
                - `HOURLY`: 1 - 23 hours
                - `DAILY`: 1 - 365 days
                - `WEEKLY`: weeks 1 - 52
                - `MONTHLY`: 1 - 12, or `FIRST, SECOND, THIRD, FOURTH, LAST, LASTDAY`
                - `ONEVENT`: XPath event query string
        - `/D <day>`: day (1-31 or `MON, TUE, WED, THU, FRI, SAT, SUN`)
        - `/M <month>`: month (default: first day of month), `JAN, FEB, MAR, APR, MAY, JUN, JUL, AUG, SEP, OCT, NOV, DEC`
        - `/I <idletime>`: how many minutes of idle time to wait for ONIDLE tasks
    - run as user: `/RU <runas_user> /RP <runas_pass>`
    - run for a period: `/ST <start-time> /ET <end-time> /SD <start-date> /ED <end-date>`

    
## Reference

- https://docs.microsoft.com/en-us/windows-server/administration/windows-commands/schtasks
- https://ss64.com/nt/sc.html
- https://www.windows-commandline.com/schedule-tasks-command-line/
- https://docs.microsoft.com/en-us/powershell/module/scheduledtasks/get-scheduledtask

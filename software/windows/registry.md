# Windows Registry


## reg command

- multi_sz: `REG ADD "..." /v "..." /t REG_MULTI_SZ /d "s1"\0"s2" /f`


## .reg files

- template:

  ```
  REGEDIT4

  [KEY1]
  "ValueName1"={Value Type}:{data}
  "ValueName2"={Value Type}:{data}
  [KEY2]
  "ValueName1"={Value Type}:{data}
  ...
  ```

- create value:

  ```
  [Key1]
  "ValueName"="Value Type"
  ```

- default value: `@ = "default value name"`
- delete key: `[-KEY1]`
- delete value: `"ValueName1"=-`
- comments: `; ...`
- value types:
  - `REG_SZ`: `"..."`
  - `REG_DWORD`: `dword:xxxxxxxx` (hex number)
  - `REG_MULTI_SZ`: `multi_sz "...", "..." ...
  - `REG_BINARY`: `hex: xx,xx,...`
  - `HEX` (all other types): `hex(typecode):xx,xx,...`
- if blocks:

  ```
  IF environment variable [= value] [!]
  [KEY1]
  "ValueName1"={Value Type}:{data}
  "ValueName2"={Value Type}:{data}
  [KEY2]
  "ValueName1"={Value Type}:{data}
  ...
  ENDIF
  ```

- escaping:

  ```
  [HKEY_LOCAL_MACHINE\key1\key2]
  "slashes\\in\\the\\name" = "slashes\\in\\the\\data"
  "quotes\"in\"the\"name" = "quotes\"in\"the\"data"
  ```

- reference: <https://docs.microsoft.com/en-us/previous-versions/windows/embedded/gg469889(v=winembedded.80)>

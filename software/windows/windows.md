# Windows


## Architecture


### Window stations

- sessions:
    - 0: used by services
    - 1+: for each user which logs on (via RDP or locally)
    - sequential IDs
    - get session ID:
        - of physically connected user (the "console session"): `WTSGetActiveConsoleSessionId()` (Vista+)
        - from process (PID): `ProcessIdToSessionId()` (Vista+)
    - a program (service) running in session 0 can pop up messages to the user using `WTSSendMessage()`
- window station: {clipboard, atom table, desktops}
    - assigned to the current session
    - the interactive window station: "WinSta0" (keyboard, mouse, display device) - same name in all sessions
    - other stations are non-interactive
    - each has three desktops:
        - Winlogon desktop
        - screen saver desktop
        - interactive desktop
    - active desktop (aka "input desktop"): `OpenInputDesktop()`


#### Session 0

- start process in session 0 interactively: `psexec -s -i 0 calc.exe`
- show session 0: `rundll32 winsta.dll,WinStationSwitchToServicesSession`
    - make sure this service is running: `sc start ui0detect`


## Security


### Users

- credentials can be cached using the `cmdkey` command (not the same as `net use`)
  - GUI: `rundll32.exe keymgr.dll,KRShowKeyMgr`


### Load DLL in all processes which load user32.dll

- needs some registry keys set
- use full path!
- don't import from anything other than kernel32.dll
- sample code:

    ```
    set name32=my32.dll
    set name64=my64.dll
    set regkey=HKLM\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Windows
    reg add "%regkey%" /v AppInit_DLLs /d c:\windows\syswow64\%name32% /f /reg:32
    reg add "%regkey%" /v AppInit_DLLs /d c:\windows\system32\%name64% /f /reg:64
    reg add "%regkey%" /v LoadAppInit_DLLs /d 1 /t REG_DWORD /f /reg:32
    reg add "%regkey%" /v LoadAppInit_DLLs /d 1 /t REG_DWORD /f /reg:64
    reg add "%regkey%" /v RequireSignedAppInit_DLLs /d 0 /t REG_DWORD /f /reg:32
    reg add "%regkey%" /v RequireSignedAppInit_DLLs /d 0 /t REG_DWORD /f /reg:64
    ```

- reference: <https://msdn.microsoft.com/en-us/library/windows/desktop/dd744762(v=vs.85).aspx>


### Change Windows AD (domain) password from Linux

- `smbpasswd -U $USER -r <DC>`
- find domain controller:
    - `DOMAIN=domain.com`
    - `dig any _kerberos._tcp.$DOMAIN`
    - `nslookup -type=a $DOMAIN`
    - `dig SRV +noall +additional _ldap._tcp.dc._msdcs.$DOMAIN | awk '{print $5}'`


### Change ACLs on registry keys

```
echo \Registry\machine\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Image File Execution Options\aswOfferTool.exe [1] >perm.tmp
regini perm.tmp
del /f /q perm.tmp
```

- permissions:

    ```
    1 (to provide Administrators Full Access)
    2 (to provide Administrators Read Access)
    3 (to provide Administrators Read and Write Access )
    4 (to provide Administrators Read, Write and Delete Access)
    5 (to provide Creator/Owner Full Access)
    6 (to provide Creator/Owner Read and Write Access)
    7 (to provide Everyone Full Access)
    8 (to provide Everyone Read Access)
    9 (to provide Everyone Read and Write Access)
    10 (to provide Everyone Read, Write and Delete Access)
    17 (to provide System Full Access)
    18 (to provide System Read and Write Access)
    19 (to provide System Read Access)
    ```

- reference: <http://www.askvg.com/windows-tip-take-ownership-permission-of-registry-keys-from-command-line/>
- if this fails: `psexec -1 -d -s C:\windows\system32\regedt32.exe` (or `-i -s` ?)


### Take ownership of file/folder and grant all to self

- `takeown /f <path> /r >nul`
- `icacls <path> /grant %username%:(OI)(CI)F /T`
    - `/T` = recursive
    - `(OI)` = object inherit
    - `(CI)` = container inherit
    - `F` = full access (`N` = no access, `M` = modify, `R` = readonly)

    
## Setup

- interesting files on ISOs:
    - `ei.cfg`: forces a specific edition (remove to enable selecting editions)
    - `cversion.ini`: removes the "upgrade" option (remove to enable upgrading vs. clean install)
    - to remove a file, toggle the deletion bit in the UDF file table
        - reference: <http://code.kliu.org/misc/winisoutils/>


## Windows version info

- `ver`
- `systeminfo`
- `wmic os get BuildNumber,BuildType,Caption,Version,Description,OperatingSystemSKU,ServicePackMajorVersion,ServicePackMinorVersion,OSArchitecture,OSProductSuite,OSType,ProductType`
- `winver` (GUI)
- `wmic os get OperatingSystemSKU` + <https://docs.microsoft.com/en-us/windows/win32/api/sysinfoapi/nf-sysinfoapi-getproductinfo>
- `reg query "HKLM\SOFTWARE\Microsoft\Windows NT\CurrentVersion" | findstr ReleaseId`


## Misc

- list scheduled tasks: `schtasks /query /v /fo CSV`
- enable "File and Printer Sharing" (SMB): `netsh advfirewall firewall set rule group="File and Printer Sharing" new enable=Yes`
    - reference: <https://support.microsoft.com/en-us/help/947709/>
- control prefetch: `HKLMACHINE\SYSTEM\CurrentControlSet\Control\Session Manager\Memory Management\PrefetchParameters`
    - value `EnablePrefetcher` (`DWORD`): 0 = disabled, 1 = app, 2 = boot, 3 = app and boot
    - value `EnableSuperfetch` (`DWORD`): same as above
- disable hibernate (free disk space): `powercfg -h off`
- run command elevated: `powershell.exe -Command "Start-Process cmd \"/k cd /d %cd%\" -Verb RunAs"`
- create + run elevated task (running doesn't need elevation): `schtasks /create /sc once /tn cmd_elev /tr cmd /rl
    highest /st 00:00` + `schtasks /run /tn cmd_elev`
- run Windows Update: `wuauclt.exe /updatenow`
- enable Linux subsystem (feature): `Enable-WindowsOptionalFeature -Online -FeatureName Microsoft-Windows-Subsystem-Linux` (`-Online` means live)
- fix quicklaunch toolbar disappearing on restart: remove folder `C:\Users\%USERNAME%\AppData\Local\TileDataLayer`


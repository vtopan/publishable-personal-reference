# Adobe Reader

- older versions: <https://www.oldapps.com/adobe_reader.php?old_adobe=17>
- silent install: `AdbeRdr812_en_US.exe /sPB /rs /l`
    - params:
        - `/sPB`: silent with progress bar; `/sAll`: silent full
        - `/rs`: suppress reboot
        - `/rps`: suppress reboot prompt
        - `/sl "LANG_ID"`: set language (code in decimal digits)
        - `/l`: log errors
        - `/ini "PATH"`: alternative initialization file
        - `/msi ...`: parameters for MSIEXEC, e.g. `/msi EULA_ACCEPT=YES REMOVE_PREVIOUS=YES ALLUSERS=1 /qn`
    - wait for installation to complete: `start "Adobe Reader Setup" /wait AdbeRdr812_en_US.exe /sPB /rs /l`



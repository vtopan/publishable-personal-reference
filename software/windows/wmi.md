# WMI

- tools:
    - `wmic`
    - *WMI Control* (`Wmimgmt.msc`, needs "Edit Security" permission)
    - `Wbemtest.exe` (can be run by non-admins if the queries don't require admin privileges)
- securing WMI / DCOM: <https://msdn.microsoft.com/en-us/library/aa393266(v=vs.85).aspx>
- setting up a remote WMI connection: <https://msdn.microsoft.com/en-us/library/aa822854(v=vs.85).aspx>
- authorize WMI users & set permissions: <https://technet.microsoft.com/en-us/library/cc771551(v=ws.11).aspx>
    - via `wmimgmt.msc`:
        - right-click *WMI Control* -> *Properties* -> *Security* tab -> "\Root\CIMV2" namespace -> *Security* button -> *Add* ...


## WMIC

- reference: <https://msdn.microsoft.com/en-us/library/aa394531(v=vs.85).aspx>


### Args

- `/format:...`: `table`, `list` (`key=value`), `xml`, `csv`, `hform` (HTML), `htable` (HTML table)


### Remarks

- "The first time you run Wmic after system installation, it must be run from an elevated command prompt."


## WMI Service

- `net stop winmgmt`
- single / shared service (port):
    - `winmgmt -standalonehost` or `winmgmt /sharedhost`, then stop/start service
    - reference: <https://msdn.microsoft.com/en-us/library/bb219447(v=vs.85).aspx>


### Configure remote access

- XP / 2003: <https://support.microsoft.com/en-us/help/875605/how-to-troubleshoot-wmi-related-issues-in-windows-xp-sp2>
- enable DCOM + WMI + outgoing connections through the firewall:
    - Vista+:
        - the group / predefined rule: `netsh advfirewall firewall set rule group="windows management instrumentation (wmi)" new enable=yes`
        - individually:
            - DCOM (TCP/135): `netsh advfirewall firewall add rule dir=in name="DCOM" program=%systemroot%\system32\svchost.exe service=rpcss action=allow protocol=TCP localport=135`
            - WMI service: `netsh advfirewall firewall add rule dir=in name="WMI" program=%systemroot%\system32\svchost.exe service=winmgmt action=allow protocol=TCP localport=any`
            - callback sink: `netsh advfirewall firewall add rule dir=in name ="UnsecApp" program=%systemroot%\system32\wbem\unsecapp.exe action=allow`
            - outgoing async commands: `netsh advfirewall firewall add rule dir=out name ="WMI_OUT" program=%systemroot%\system32\svchost.exe service=winmgmt action=allow protocol=TCP localport=any`
    - 2003 / XP: 
        - `netsh firewall set service remoteadmin enable`
        - `netsh firewall set service remoteadmin enable subnet`
        - `netsh firewall set service remoteadmin enable custom <IP>,LocalSubnet`
- enable/start remote registry as well: `net start RemoteRegistry`
- CIMOM settings
    - computers which are not in the same domain or in trusted domains need anonymous access enabled:
        - `reg add HKLM\SOFTWARE\Microsoft\WBEM\CIMOM /v AllowAnonymousCallback /t REG_DWORD /d 1 /f` (set to 0 to disable)
- add the remote host to trusted hosts (via PowerShell): `cd wsman:\localhost\client`, `set-item trustedhosts <hostname>`
- test using PowerShell: `Get-WmiObject -Namespace "root\cimv2" -Class Win32_LogicalDisk -ComputerName <REMOTE_IP> -Credential <DOMAIN\User>`
- reference:
    - <https://iphostmonitor.com/kb/remote-wmi-monitoring.html>
    - <https://msdn.microsoft.com/en-us/library/aa822854(v=vs.85).aspx>
    - <https://www.poweradmin.com/help/faqs/how-to-enable-wmi-for-remote-access/>

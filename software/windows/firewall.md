# Windows Firewall

## Examples

- allow port: `netsh firewall add portopening TCP 12345 RuleName`
- delete rule by name: `netsh advfirewall firewall delete rule name="DCOM"`

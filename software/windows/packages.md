# Installing Software in Windows


## Unattended installation

- automated tool: `Ninite` (e.g. [Ninite Avira Installer](https://ninite.com/avira/))
- AVG Free AV: `/silent=true` (use `start "..." /wait ...` to wait for it)
- Adobe Reader: `AdbeRdr812_en_US.exe /sPB /rs /l`
- Avast: `/SILENT` (preferrably run the kit, copy the contents of `%temp%/_av_iup.tm~*` except for `aswOfferTool.exe`)
    and run `instup.exe /edition:1 /prod:ais /sfx /sfxstorage:%temp%\_av_iup.tm~a02044`; might have to include the
    actual UUIDs (extract them from the command line, e.g.`/ga_clientid:958e924c-f36c-4b7d-920e-76e64068097d` and
    `/guid:9d3474bd-45aa-49c4-9442-d4421dfdbd02`)
    - official switches: `/VERYSILENT /NORESTART /SP-`
    - prevent Google Chrome "offer":
        - before:

            ```
            reg add "HKLM\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Image File Execution Options\aswOfferTool.exe" /v Debugger /d logonui.exe /f
            reg add "HKLM\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Image File Execution Options\GoogleUpdate.exe" /v Debugger /d logonui.exe /f
            ```

        - run unattended install: `start /wait avast_free_antivirus_setup_offline.exe /VERYSILENT /NORESTART /SP-`
        - after:

            ```
            reg delete "HKLM\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Image File Execution Options\aswOfferTool.exe" /f
            reg delete "HKLM\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Image File Execution Options\GoogleUpdate.exe" /f
            ```

        - idea source: <http://www.msfn.org/board/topic/159026-avast-free-antivirus-v70xxxx-silent-install/>


## List installed Windows features

- `wmic /namespace:\\root\cimv2 path win32_optionalfeature where "InstallState = 1"`
- `powershell -Command "foreach ($feature in Get-WmiObject -Class Win32_OptionalFeature -Namespace root\CIMV2 -Filter \"InstallState = 1\") {$feature.Name}"|sort`

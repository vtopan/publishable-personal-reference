# Windows services

- GUI: `services.msc` or `msconfig`
- registry path: `HKLM\SYSTEM\CurrentControlSet\services`
    - startup type (`Start` value):
        - Automatic: 2
        - Manual: 3
        - Disabled: 4
        - Automatic (Delayed Start): 2


## Commands

- start/stop: `net start/stop winmgmt`
- configure via registry: `REG add "HKLM\SYSTEM\CurrentControlSet\services\RemoteRegistry" /v Start /t REG_DWORD /d 2 /f`
- set executable: `sc.exe config service_name binPath= c:\svc.exe type= own`


## Troubleshooting

### svchost / wuaueng.dll eats 100% CPU (Windows Update)

- problem: possibly corrupt update DB
- fix:

    ```
    net stop wuauserv
    rd /s /q c:\Windows\SoftwareDistribution
    net start wuauserv
    ```

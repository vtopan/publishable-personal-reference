# Linux Configuration


## Misc


### Add CA to trusted root

- "official" storage path: `/etc/ssl/certs`
- cert should be a `.crt` for `/usr/share/` and `.pem` for `/usr/local/` 
- `apt install ca-certificates`
- `cp cacert.crt /usr/share/ca-certificates` (distro) or to `/usr/local/share/ca-certificates/` (local)
- `dpkg-reconfigure ca-certificates` or `update-ca-certificates` (non-interactive)
- for browsers which don't use the root cert store:
    - `certutil -d sql:$HOME/.pki/nssdb -A -t "C,," -n "My Homemade CA" -i /path/to/CA/cert.file`
- only use cert in `curl`: `curl --cacert /path/to/CA/cert.file https://...`
- test connect with openssl: `openssl s_client -connect foo.whatever.com:443 -CApath /etc/ssl/certs`

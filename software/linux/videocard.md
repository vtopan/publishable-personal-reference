# Video Card Setup in Linux

- *nvidia-prime* and *bumblebee* are alternative Optimus (dual card) toggling tools
- *PRIME* = the Linux name for Optimus (nVidia) / AMD DSG drivers / technology
- *Mesa* = open source OpenGL implementation


## Configuring Optimus (dual Intel on-chip + Nvidia) cards

- (non-clean install) remove all nvidia stuff:
    - `sudo apt purge xorg* xserver-xorg* nvidia* bumblebee*`
    - `sudo apt install xorg xserver-xorg` (note: this pulls the Nouveau driver along)
- check if nouveau is installed: `lsmod|grep nouv`
    - remove nouveau:
        - stop km: `sudo rmmod nouveau` (from C-A-F1)
        - `sudo apt purge xserver-xorg-video-nouveau`
- (optional?) install bbswitch to turn off the discrete GPU:
    - `sudo apt install dkms bbswitch-dkms`
    - stop GPU: `sudo modprobe bbswitch load_state=0`
    - check: `cat /proc/acpi/bbswitch` (should be OFF)
- identify card & find recommended video driver version from <http://www.nvidia.com/Download/index.aspx?lang=en-us>
    (don't download, just get the first part of the version, e.g. *384* from `384.98`)
    - in the following lines, I'll use 384 for command examples; replace as necessary
- add proprietary graphics drivers ppa: `sudo apt-add-repository ppa:graphics-drivers/ppa` + `apt update`
- install driver: `sudo apt update; sudo apt install nvidia-387 nvidia-settings bumblebee-nvidia primus`
    - don't use `--no-install-recommends` without adding `xorg-xserver-legacy`
        - may want `libcuda1-387` `nvidia-opencl-icd-387`
    - `bumblebee-nvidia` will automatically:
        - remove some `xserver-xorg-video-*` packages
        - pull a (wrong) version of `nvidia-*` along unless the correct one is explicitly selected
        - blacklist the nvidia driver (check with `tail /etc/modprobe.d/bumblebee.conf` -
            lines like `blacklist nvidia-384` should show up) - this is ok
    - in `/etc/bumblebee/bumblebee.conf`:
        - `[bumblebeed]`: `Driver=nvidia`
        - `[optirun]`: `Bridge=primus` (optional: `VGLTransport=rgb` ?)
        - `[driver-nvidia]`:
            - `KernelDriver=nvidia-387`
    - create `nvidia-current` softlinks in `/usr/lib` AND `/usr/lib32` to `nvidia-387`
    - restart: `sudo systemctl restart bumblebeed`
    - blacklist drivers on boot in `/etc/modprobe.d/bumblebee.conf`:
        - `blacklist <driver>`: `nvidia_387` (also `_drm`, `_modeset`, `_uvm`), `nvidiafb`
- install mesa + libs:
    `sudo apt install mesa-utils mesa-utils-extra libgl1-mesa-glx libgl1-mesa-glx:i386 nvidia-driver-libs-i386`
- `systemctl disable nvidia-persistenced` (not much help, it is started by lightdm, then complained about)
- use `sudo nvidia-settings` to set the Intel card as default
- select mesa as GL provider (choose the entry with "mesa" in the title for each setting):
    - automatically:

        ```
        sudo update-alternatives --set x86_64-linux-gnu_gl_conf /usr/lib/x86_64-linux-gnu/mesa/ld.so.conf
        sudo update-alternatives --set i386-linux-gnu_gl_conf /usr/lib/i386-linux-gnu/mesa/ld.so.conf
        sudo update-alternatives --set x86_64-linux-gnu_egl_conf /usr/lib/x86_64-linux-gnu/mesa-egl/ld.so.conf
        ```

    - or manually:

        ```
        sudo update-alternatives --config i386-linux-gnu_gl_conf
        sudo update-alternatives --config x86_64-linux-gnu_egl_conf
        sudo update-alternatives --config x86_64-linux-gnu_gl_conf
        ```

- the information above is gathered from several (partially conflicting) sources and it likely
    won't work, see below for references


## Troubleshooting

- `optirun glxinfo` => "[ERROR]Cannot access secondary GPU - error: Could not load GPU driver"
    - `/etc/bumblebee/bumblebee.conf` has an invalid driver name in `[driver-nvidia]`
    - change `KernelDriver=nvidia` to the actual version, e.g. `nvidia-387`


## Get information / debug

- debug GL:
    - `LIBGL_DEBUG=verbose glxgears`
    - `optirun glxspheres64`
    - `glxinfo | grep "version|render"` ("OpenGL renderer")
- list video cards: `lspci | egrep 'VGA|3D'`
- get current / compatible driver information on Ubuntu: `sudo ubuntu-drivers devices`
- get driver used by Xorg: `xrandr --listproviders` (first = Intel onboard, second = discrete GPU)
    - `modesetting` is the Intel driver
- get prime card in use: `sudo prime-select query` (for nvidia-prime)
- get bbswitch status: `cat /proc/acpi/bbswitch` (ON => nvidia card is in use)
- list X clients: `xlsclients`
- nvidia status: `nvidia-smi` or (if installed) `nvidia-settings` - prefix with `optirun` if needed
- read GPU memory size: `grep Memory: /var/log/Xorg.0.log`


## Misc

- add 32 bits subsystem: `sudo dpkg --add-architecture i386`
- applet: `nvidia-power-indicator`
- stop display manager: `sudo service lightdm stop` (or `gdm` / `mdm`)
- set default card in prime: `prime-select intel` (or `nvidia`)
- fix libgl stuff:

    ```
    apt install --reinstall libgl1-mesa-glx libgl1-mesa-glx:i386
    apt install mesa-utils nvidia-driver-libs-i386
    ```



## Reference

- <https://lenovolinux.blogspot.com/2016/05/bumblebee-on-lenovo-t440p-nvidia-gt.html>
- <https://www.pcsuggest.com/nvidia-optimus-ubuntu/>
- <https://wiki.archlinux.org/index.php/PRIME>
- <https://wiki.archlinux.org/index.php/NVIDIA_Optimus>
- <https://wiki.archlinux.org/index.php/Bumblebee>
- <https://wiki.freedesktop.org/nouveau/Optimus/>

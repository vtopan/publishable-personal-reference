# systemd


## Misc

- wait for GUI: `After=display-manager.service` in `[Unit]`


## Sample service (unit)

```
[Unit]
Description=Log watcher + notify-send
After=display-manager.service

[Service]
ExecStart=/bin/bash /usr/bin/watchlog
KillMode=process
Restart=on-failure
RestartSec=10s
```


## Reference

- <https://bbs.archlinux.org/viewtopic.php?id=205867>
- <https://wiki.archlinux.org/index.php/Systemd>
- <https://www.freedesktop.org/software/systemd/man/systemd.service.html>
- all systemd man pages: <http://0pointer.de/public/systemd-man/>
- <https://www.digitalocean.com/community/tutorials/understanding-systemd-units-and-unit-files>

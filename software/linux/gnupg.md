# GnuPG


## Setup

- `sudo apt-install gnupg`


## Ops

- generate key: `gpg --gen-key`
- list keys: `gpg -k` (public), `gpg -K` (private)
- export keys (use `-a` to get `.asc` ASCII dumps):
    - public: `gpg --export ID > pubkey.pgp`
    - private: `gpg --export-secret-keys ID > private-key.pgp`
- decrypt to stdout: `gpg --decrypt filename`
- import key: `gpg --import key` (use `--allow-secret-key-import` for private keys)
- dump `.pgp` contents: `gpg --list-packets file.pgp` (keys and messages) or `pgpdump file`

# Linux Networking


## Mounting shares

- check access:
    - `smbclient -L host -U user%pass`
        - `-c cmd1;cmd2;...` - run commands (`cd`, `ls`, `get <filename> [<localname>]`, `md <name>`,
            `put <local> [<remotename>]`, `rd <dir>`, `rm <file>`)
- NetBIOS name lookup (by IP): `nmblookup -A 192.168.1.1`


### Mount network share

- using `mount.cifs`:
    - install package `cifs-utils` (it won't prompt for the password without those) - formerly `smbfs` on
        Ubuntu up to 10.04
    - `sudo mount -t cifs -o rw,nounix,username=<user>,domain=<domain>,password=<pass> //<ip>/<path> /mnt/<point>`
        - `-o nounix` disables many Linux-specific mount options (see `man mount.cifs`)
    - `<domain>` is not required; the `<password>` is prompted for if not given
    - needs root (to allow `-o`)
    - credentials in a filename: `-o credentials=filename`, the file should contain the lines `username=...`,
        `password=...` (as given in `-o` options)
    - unmounting: `sudo umount /mnt/<point>` or `sudo umount //<ip>/<path>`
- using `gvfs-mount` (Gnome):
    - `gvfs-mount smb://user@server/storage`
- using `smbnetfs`:
    - `smbnetfs <localpath>`
    - setup:
        - `~/.smb/`:
            - `smb.conf` (copy `/etc/samba/smb.conf`)
            - `smbnetfs.conf` (copy `/etc/smbnetfs.conf`)
    - credentials: `.smb/smbnetfs.conf` -> `auth user "pass"`
- fstab: `//<ip>/<path> /mnt/<point> cifs noauto,username=<user>,iocharset=utf8,rw,user 0 0`
    - `-o user` to allow the user to mount it
- Python notes:
    - `os.rename()` can't move files across mountpoints (`rename()` can't)
    - `shutil.move()` uses `shutil.copy2()`, which calls `shutil.copystat()` which may fail; use `shutil.copy()` +
        `os.unlink()` or replace `shutil.copy2()` with `shutil.copy()` at runtime - not threadsafe (see
        <https://stackoverflow.com/questions/26654097/move-from-local-linux-folder-to-a-windows-share-mounted-with-cifs>)


## Get info

- parse `ip` output (to json):

    ```sh
    if which ip 2>&1 >/dev/null; then
        ip a l|while read line; do
            if echo $line|grep -qP '^\d+:'; then
                pfx=
                if test -n "$ifname"; then printf "\n$p$p],"; fi
                ifname=`echo $line|awk '-F[ :]+' '{ print $2 }'`
                printf "\n$p$p'$ifname': ["
            elif echo $line|grep -qP '^link/'; then
                mac=`echo $line|cut -d' ' -f2`
            elif echo $line|grep -qP '^inet'; then
                addr=`echo $line|cut -d' ' -f2`
                printf "$pfx\n$p$p$p{'mac': '$mac', 'ip': '$addr'}"
                if test -z "$pfx"; then pfx=,; fi
            fi
        done
    fi
    ```

- parse `ifconfig` output to json:

    ```sh
    ifconfig|while read line; do
        if echo $line|grep -q ' Link \| flags='; then
            pfx=
            if test -n "$ifname"; then printf "\n$p$p],"; fi
            ifname=`echo $line|grep -o '^[a-z0-9]\+'`
            printf "\n$p$p'$ifname': ["
            mac=
        fi
        if echo $line|grep -q ' HWaddr \|ether '; then
            mac=`echo $line|grep -io "[a-f0-9]\{2\}:[a-f0-9:]\+"|head -1`
        elif echo $line|grep -q '^inet'; then
            if echo $line|grep -q '^inet '; then
                addr=`echo $line|grep -o "[0-9]\+\.[0-9]\+\.[0-9]\+\.[0-9]\+"|head -1`
            else
                addr=`echo $line|grep -io "[a-f0-9]*::[a-f0-9/]\+"|head -1`
            fi
            mask=`echo $line|grep -io "mask[ :][a-fx0-9.]\+"|head -1|awk '-F[ :]' '{ print $2 }'`
            printf "$pfx\n$p$p$p{'mac': '$mac', 'ip': '$addr', 'netmask':'$mask'}"
            if test -z "$pfx"; then pfx=,; fi
        fi
    done
    ```


## Disable dnsmasq

- edit `/etc/NetworkManager/NetworkManager.conf` and comment out `dns=dnsmasq` (with a `#`), then restart the
    `network-manager` service and presto: back to a normal `/etc/resolv.conf`

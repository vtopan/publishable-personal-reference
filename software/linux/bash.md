# bash


## History

- example setup:

    ```sh
    export HISTCONTROL=ignoredups:erasedups
    export HISTSIZE=1000000
    export HISTFILESIZE=1000000
    shopt -s histappend
    # don't add y * commands to history
    HISTIGNORE='y:y *'
    ```
	
	- `HISTCONTROL` values:
		- `ignorespace` – ignore commands starting with spaces
		- `ignoredups` – ignore (consecutive) duplicate commands
		- `ignoreboth` – ignorespace + ignoredups
		- `erasedups` – ignore all duplicates (not just consecutive)

- previous command:
    - re-run: `!!` or `!-1` or `fc -s -- -1`
    - see/get: `history -p \!-1` or `fc -l`


## Variables

- replace substring in variable: `${varname//pat/subst}` (the double slash means replace all occurences)
- `LC_ALL`: used by text processing (e.g. to interpret special character codes)
    - `LC_ALL=en_US.UTF-8`
    - alternative: `echo "en_US.UTF-8 UTF-8" >/etc/locale.gen; locale-gen`


## Misc

- clipboard: `xclip` (`xclip -o` to read)


### Bash completion

- *readline* library (see also `man readline`)
- `.inputrc`:
    - ignore case on TAB completion: `set completion-ignore-case on`
    - show all options on first TAB press: `set show-all-if-ambiguous on`
    - note: settings can also be placed in `.bashrc` enclosed in `bind "..."`

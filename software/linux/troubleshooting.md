# Troubleshooting Linux


## Unset TERM ?

- error: ``The TERM environment variable is unset!``
- solution: `export TERM=xterm`


## GTK pixmap error

- error: `Gtk-WARNING **: Unable to locate theme engine in module_path: "pixmap"`
- solution: `sudo apt-get install gtk2-engines-pixbuf`


## Broken text display in Linux after sleep

- error: text display is messed up, some letters missing, some changed
- solution: restart X: `sudo service lightdm restart` (close all apps before!)


## Missing man pages for posix

- error: `No manual entry for pthread_mutex_init`
- solution: `sudo apt install manpages-posix manpages-posix-dev`


## Ubuntu setup apt-cdrom-setup error in VM

- error: during the Ubuntu setup, "Failed to Load installer components. Loading apt-cdrom-setup failed for unknown reasons. Aborting."
- problem: not enough RAM for the VM
- solution: give the VM more ram (v13: ~102, v14: ~165, v16: > 256 etc.)


## Locale error on `apt install`
    
- error: `perl: warning: Setting locale failed.`, `perl: warning: Please check that your locale settings`
- solution: set `LC_ALL=en_US.UTF-8` in `.bashrc`
    - may need `apt-get install debconf; dpkg-reconfigure locales`

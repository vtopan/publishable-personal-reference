# Common Linux Commands

## Misc tips & tricks

- multi-line sed / grep: `-z` (treat '\n' as '\0')
- render HTML to shell: `...|w3m -T text/html -dump` (or similarly with `lynx`)
- colorize command output / various languages: `...|pygmentize`


## File-related commands


### Unpacking installer files

- generic: 
    - package *cabextract*: `cabextract ...`
    - package *p7zip*: `7z x ...` 
- InstallShield - package *unshield*: `unshield x ...`


### rsync

- examples:
    - rsync over SSH, recursive, update only: `rsync -ruessh src1 src2 remote:/path`
        - `-R` (relative) makes it use full paths from source as relative paths from the remote base folder


## Disk-related Commands


### Guess partition type

- `sudo file -Ls /dev/sda1`


### ExFAT support

- `sudo apt install exfat-fuse exfat-utils`


### Mounting files & devices

- mount .vmdk:

    ```sh
    # apt-get install kpartx
    # kpartx -av disk.vmdk
    add map loop0p1 (254:0): 0 716800 linear /dev/loop0 2048
    add map loop0p2 (254:1): 0 15037324 linear /dev/loop0 21148
    # mkdir /mnt/vmdkpart
    # mount -o ro /dev/mapper/loop0p2 /mnt/vmdkpart
    ```

- mount .vdi:

    ```
    sudo apt install qemu-tools
    sudo modprobe nbd
    sudo qemu-nbd -c /dev/nbd0 <vdi-path>
    mount /dev/nbd0p1 <mount-path>
    ```

    - to unmount: `sudo umount <mount-path>; sudo qemu-nbd -d /dev/nbd0`

- mount initrd: `mount -t sysfs /initrd.img /temp -o loop`


## Misc Commands


### xxd

- hex dumper
- args:
    - `-e` - little endian
    - `-c#` - `#` bytes per line
    - `-g#` - group `#` bytes in hexdump

    
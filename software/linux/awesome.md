# Awesome WM

- control panel: `unity-control-center`


## Misc

- keep spawned children on the same screen if spawned by windows on the second screen:
    `{ rule = { class = "Chromium-browser" }, ..., callback = awful.client.movetoscreen, ... }`


## Systray apps

- HP: `/usr/bin/python3 /usr/bin/hp-systray -x`

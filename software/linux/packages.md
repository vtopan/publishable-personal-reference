# Linux packages


## Packages

- guest tools:
    - VMWare: `open-vm-tools`
    - VirtualBox: `virtualbox-guest-{dkms,utils,x11}`


## List installed packages

- all installed packages:
    - ``dpkg --get-selections``
    - ``dpkg -l | grep ^ii | sed 's_  _\t_g' | cut -f 2 > installed-pkgs``

- list all non-default installed packages:
    - ``( zcat $( ls -tr /var/log/apt/history.log*.gz ) ; cat /var/log/apt/history.log ) | egrep '^(Start-Date:|Commandline:)' | grep -v aptdaemon | egrep '^Commandline:'``
    - ``aptitude search '~i!~M'``
    - get all with "Auto-Installed: 0" from ``/var/lib/apt/extended_states``

- include first run:
    - ``( zcat $( ls -tr /var/log/apt/history.log*.gz ) ; cat /var/log/apt/history.log ) | egrep '^(Start-Date:|Commandline:)' | grep -v aptdaemon | egrep -B1 '^Commandline:'``


## Misc

- unattended installation (e.g. for tshark/wireshark dumpcap "run as root?" questions):
    `DEBIAN_FRONTEND=noninteractive apt-get install -y ...`

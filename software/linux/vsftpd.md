# vsftpd


## Configuration

- sample configuration (`/etc/vsftpd.conf`):

    ```
    pam_service_name=vsftpd

    listen=YES
    listen_ipv6=NO

    anonymous_enable=NO
    write_enable=YES

    local_enable=YES
    hide_ids=YES
    #guest_enable=YES
    #guest_username=ftp
    #user_sub_token=$USER

    userlist_deny=NO
    userlist_enable=YES
    userlist_file=/etc/vsftpd.users
    chroot_local_user=YES
    chroot_list_enable=NO
    #allow_writeable_chroot=YES

    local_root=/var/ftp

    ftpd_banner=Yello!
    ```

    - `/etc/vsftpd.users` should contain the allowed local users, one per line
    - the root folder (chroot) must not be writable by ANYBODY!
- additional relevant settings:
    - `/etc/pam.d/vsftpd`:

        ```
        auth required pam_pwdfile.so pwdfile /etc/vsftpd.passwd
        account required pam_permit.so
        ```

        - install `libpam-pwdfile`
        - comment out "auth required pam_listfile.so ..." and "... pam_shells.so ..."
        - use `openssl passwd -1` to generate passwords for the .passwd file (format: `user:hashed_password`, don't just
            use the openssl output!)
        - `httpasswd` (in `apache2-utils`) generates Apache-specific MD5s which are incompatible with *vsftpd* and `-d`
            (crypt) passwords are weak (max. len. 8)

    - `/etc/vsftpd.conf` must be owned by root
    - the `ftp` user must exist, e.g. `ftp:x:1001:1001:FTP user:/ftp:/usr/bin/rssh`, and it must own its home folder
- use `curl -v ftp://user:pass@host/` to test
    - test upload: `echo test>test.txt; curl -v ftp://user:pass@host/write/ -T test.txt; rm test.txt`
- the "userlist_enable" option only controls what usernames are allowed/denied (the validation is performed before
    asking for a password)


# Wine


## Misc

- create 32 bit prefix: `WINEPREFIX="$HOME/.wine32" WINEARCH=win32 wine wineboot`
- get latest *winetricks*:

    ```
    wget  https://raw.githubusercontent.com/Winetricks/winetricks/master/src/winetricks
    chmod +x winetricks
    sudo mv -v winetricks /usr/local/bin
    ```

- run 32 bits winetricks: `WINEPREFIX="$HOME/.wine32" WINEARCH=win32 winetricks`

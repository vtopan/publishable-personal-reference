# Apparmor

## Setup

- `sudo apt-get install apparmor-profiles apparmor-utils`


## Usage

- profiles: `/etc/apparmor.d/`
- create blank profile: `sudo aa-autodep nginx`
- put in complain mode: `sudo aa-complain nginx`
- approve/disapprove complaints from log interactively: `sudo aa-logprof`
- enforce profile: `sudo aa-enforce nginx`


## Common settings

- network access granted by default in `abstractions/nameservice`
- basic access:
    - `/etc/passwd r,`
    - `/etc/* r,`
    - `/etc/** w,`
	- `/tmp/myprog.* l,` (links)
	- `/bin/mount ux,` (`ux` = unconstrained execute, `px` = profile, `ix` = inherit)
- capabilities (`capability setuid,`): `dac_override`, `dac_read_search`, `net_bind_service`, `setgid` etc.
- template (use `aa-autodep`!):

    ```
	#include <tunables/global>

	/usr/bin/curl flags=(complain) {
		#include <abstractions/base>
		#include <abstractions/nameservice>

		/** w,
		/usr/bin/curl mr,
	}
    ```

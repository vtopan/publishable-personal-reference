# byobu

Terminal replacement (tmux-based).


## Settings

- change byobu backend (tmux -> screen): ``byobu-select-backend``
- unbind F2 (to use it in *vim*): `unbind-key -n F2` in `~/.byobu/keybindings.tmux`
- fix Ctrl+Left/Right:
    - in `~/.inputrc`: `"\e[1;5C": forward-word` and `"\e[1;5D": backward-word`
    - in `~/.byobu/.tmux.conf` (this may not work / be necessary): `set-window-option -g xterm-keys on`
    - in `~/.byobu/keybindings.tmux`: `unbind-key -n C-Left` and `unbind-key -n C-Right`



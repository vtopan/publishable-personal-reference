# Ubuntu


## Privacy

- disable crash report uploading (to prevent sensitive data upload):
    - `gsettings set com.ubuntu.update-notifier show-apport-crashes false` (disables the GUI popup)
    - `sudo apt purge whoopsie` (`whoopsie` uploads the crashes; `apport` can also be removed altogether, since it
        generates the crash reports)
    - DON'T ~~sudo apt purge python3-apport~~ - it also removes `ubuntu-desktop` (the GUI won't take input after that)
    - manually disable telemetry & crash submission in *Settings* -> *Privacy* (the name varies between versions)
    - reference:
        - [Apport @ Ubuntu wiki](https://wiki.ubuntu.com/Apport)
        - [Design @ Ubuntu wiki](https://wiki.ubuntu.com/ErrorTracker)
- disable systemd-resolv (and close TCP:5355):

    ```bash
    grep -q 'LLMNR=no' /etc/systemd/resolved.conf || echo 'LLMNR=no' >> $_
    systemctl restart systemd-resolved.service
    ```

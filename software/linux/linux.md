# Linux


## Logging / user messages

- pop up system message (notification): `notify-send "summary" "body"`
    - `-t ...`: expire time in ms
    - `-u ...`: urgency (low / normal / critical)
    - `-i ...`: icon
    - to use in cron, set the env var `DBUS_SESSION_BUS_ADDRESS` manually
- write message to kernel log (`dmesg`): `sudo sh -c "echo hello >/dev/kmsg"` (write to `/dev/kmsg`)
- write message to `syslog`: `logger "message"`


## Misc

- add software "alternative": `sudo update-alternatives --install /usr/bin/vim vim /usr/bin/nvim 60`
    - select alternative: `sudo update-alternatives --config vim`
- find where STDERR is redirected for e.g. `awesome`: `ls -l /proc/$(pidof awesome)/fd/2`
- don't sleep/turn off laptop when closing the lid: edit `/etc/systemd/logind.conf`, set:

    ```
    HandleLidSwitch=ignore
    HandleLidSwitchDocked=ignore
    ```

    - then restart the `systemd-logind` service: `systemctl restart systemd-logind.service`
    - other, unchecked solution: `gconftool-2 --type string --set /apps/gnome-power-manager/buttons/lid_ac "nothing"`

- open any file with the associated app: `gnome-open` - `apt install libgnome2-bin`
- NTP sync date/time: `sudo ntpdate europe.pool.ntp.org` (`apt install ntpdate`)
    - sample IP: `162.23.41.56`
- screenshots:
    - full screen: `scrot -z 'screen-%y%m%d-%H%M%S-$wx$h.png' -e 'mv $f ~/screenshots/ 2>/dev/null'`
    - window: `scrot -z -bu 'screen-%y%m%d-%H%M%S-$wx$h-window.png' -e 'mv $f ~/screenshots/ 2>/dev/null'`
    - alternative: `import -screen ~/screenshots/screen-" .. os.date("%y%m%d-%H%M%S") .. "-window.png")` (needs
        `imagemagick`)
    - alt window: `import -window $(xprop -root|grep '_NET_ACTIVE_WINDOW(WINDOW)'|cut -d\# -f2) /tmp/screen.png`
    - first XINERAMA head (actual stuff displayed on a monitor in a multi-mon setup):

        ```
        xdpyinfo -ext XINERAMA | sed '/^  head #/!d;s///' |head -n1|IFS=' :x@,' read i w h x y; import -screen -crop ${w}x$h+$x+$y /tmp/head-$i.png
        ```

    - all XINERAMA heads:

        ```
        xdpyinfo -ext XINERAMA | sed '/^  head #/!d;s///' |
        while IFS=' :x@,' read i w h x y; do
            import -window root -crop ${w}x$h+$x+$y head_$i.png
        done
        ```

    - reference: <https://wiki.archlinux.org/index.php/Taking_a_Screenshot>

- set timezone:
    - interactively: `dpkg-reconfigure tzdata`
    - noninteractively: `sudo timedatectl set-timezone UTC` (or `Europe/London`)
- active window ID: `xprop -root|grep '_NET_ACTIVE_WINDOW(WINDOW)'|cut -d\# -f2`
- display image in terminal:
    - as text: `cacaview img.png` (`apt install caca-tools`)
    - as image: `fim img.png` (`apt install fim`)
    - default gnome viewer: `eog`
- save `alsamixer` volume levels: `sudo alsactl store`
- command autocompletion (`/etc/inputrc` or `~/.inputrc` or `bind "set ..."` in `~/.bashrc`):
    - case insensitive: `set completion-ignore-case on`
    - show choices on the first tab when ambiguous:  `set show-all-if-ambiguous on`
    - **note**: if a `~/.inputrc` is created, it must explicitly source `/etc/bashrc`: `$include /etc/inputrc`
- watch network bandwitdh usage: `bmon` (`apt install bmon`)
- no password sudo: `username ALL=(ALL) NOPASSWD:ALL` (or just a binary: `NOPASSWD:/sbin/shutdown`)
    - alternative: `username ALL=NOPASSWD:ALL` (this will still require a password for `sudo -u otheruser`)
    - make sure it's the last line that applies to `username` (they are applied in order)
- disable IPv6: `net.ipv6.conf.eth0.disable_ipv6 = 1` in `/etc/sysctl.conf`
- keyboard layout: `setxkbmap ro -variant std` for Romanian, `setxkbmap us` to go back to US layout
- printer setup:
    - `gksudo hp-setup` to add
    - `system-config-printer` to manage / remove
    - scan network for (HP) printers: `nmap -p 9100,515,631 -sV -v -T4 --open -sC 192.168.0.0/24`
- list of running processes' full paths: `sudo ls /proc/*/exe|xargs readlink`
- map application names to commands:
	- `grep -hiP '^(exec|name)=' /usr/share/applications/|less`
- toggle application autostart: `gnome-session-properties`
	- show hidden startup apps: `sudo sed -i 's/NoDisplay=true/NoDisplay=false/g' /etc/xdg/autostart/*.desktop`
- show keyboard codes for keypresses: `showkey -a` (works through ssh)


## Video mode

- video mode IDs (VESA):
    - 32bpp: 1024x768 = 792, 1152x864 = 356, 1280x1024 = 795, 1600x1200 = 799, 1440×900 = 868, 1600x900 = 0x34d
- available video modes in Linux: `sudo hwinfo --framebuffer` or `sudo fbset -i` (`hwinfo` / `fbset` packages)
- to list all known VBE modes, boot into grub console (press `C` in grub menu) and run `set pager=1`, `vbeinfo`, `reboot`
- to set the video mode for grub:
    - `/etc/default/grub`: `GRUB_GFXMODE=1280x1024`, `GRUB_GFXPAYLOAD_LINUX=keep`
    - `sudo update-grub`
- when booting, Linux will most likely ignore the grub mode (even with `nomodeset` in the commandline); `795` worked

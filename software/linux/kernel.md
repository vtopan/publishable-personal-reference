# Linux Kernel


## Misc


### Signing drivers (e.g. virtualbox) on UEFI + Secure Boot

```sh
### create dir
sudo mkdir ~/.somewheresafe; cd $_

### generate MOK; give a pass; then generate a temporary passwordless key
sudo openssl req -new -x509 -newkey rsa:2048 -keyout MOK.priv -outform DER -out MOK.der -days 36500 -subj "/CN=io-mok/"
sudo openssl rsa -in MOK.priv -out tmp.key

### set K... to the pass
export KBUILD_SIGN_PIN
read -p "Please enter key passphrase (leave blank if not needed): " KBUILD_SIGN_PIN

### check sign mode (hash) used by the kernel
sudo grep CONFIG_MODULE_SIG_HASH /boot/config-*

### sign kernel modules
for m in vboxdrv vboxnetflt vboxnetadp vboxpci; do
    sudo /usr/src/linux-headers-$(uname -r)/scripts/sign-file sha512 ./tmp.key ./MOK.der $(modinfo -n $m)
done

### shred temp private key
sudo shred -vfuz tmp.key

### check if a module is signed
for m in vboxdrv vboxnetflt vboxnetadp vboxpci; do
    hexdump -C $(modinfo $m|head -n1|awk '{print $2}')|tail -n10
done

### modprobe if the key is already imported
for m in vboxdrv vboxnetflt vboxnetadp vboxpci; do
    sudo modprobe $m
done

### import MOK - remember the password given here until after reboot
sudo mokutil --import MOK.der

### reboot to enroll MOK: "Enroll MOK" -> "Continue" -> "Yes" -> "OK"
sudo reboot
```

- references:
    - <https://kb.vmware.com/kb/2146460>
    - <https://askubuntu.com/questions/770205/how-to-sign-kernel-modules-with-sign-file>
- rebuild / reconfigure VirtualBox kernel modules: `sudo apt-get --reinstall install virtualbox-dkms`


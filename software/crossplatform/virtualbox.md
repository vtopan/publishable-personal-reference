# VirtualBox


## Setup


### Host

- install VirtualBox + ExtPack: `apt install virtualbox virtualbox-ext-pack`


#### Networking

- create NAT network: `VBoxManage natnetwork add --netname lap-natnet --network 172.1.1.0/24 --enable --dhcp on`
- start NAT net. service: `sudo VBoxManage natnetwork start --netname lap-natnet`
- manual NAT setup:
    - `sudo apt-get install uml-utilities dnsmasq`

    ```
    auto vbox0
    iface vbox0 inet static
        address 10.0.0.254
        netmask 255.0.0.0
        pre-up /usr/sbin/tunctl -u USERNAME -t vbox0 >/dev/null
        up /usr/sbin/dnsmasq --interface=vbox0 --except-interface=lo \
            --bind-interfaces --user=nobody \
            --dhcp-range=vbox,10.0.0.100,10.0.0.200,255.0.0.0,10.255.255.255,8h \
            --domain=vbox.lan --pid-file=/var/run/dnsmasq.pid --conf-file
        up iptables -t nat -A POSTROUTING -o IFACE -j MASQUERADE
        down iptables -t nat -D POSTROUTING -o IFACE -j MASQUERADE
        down kill -9 `cat /var/run/dnsmasq.pid` && rm /var/run/dnsmasq.pid
    post-down /usr/sbin/tunctl -d vbox0 >/dev/null
    ```

    - `sudo /etc/init.d/networking start`
    - taken from <https://wiki.ubuntu.com/VirtualBoxNetworking>
- reference: <https://www.virtualbox.org/manual/ch06.html>


### In VM

- install guest tools: `apt install virtualbox-guest-dkms` (pulls everything after itself)
- for SSH access to guest add a host-only NIC & set it up in `/etc/network/interfaces`


### Programming / automating the VM

- `vboxmanage` subcommands:
    - list VMs & GUIDs: `list vms`
    - VM info: `showvminfo <vm> [--machinereadable]` (e.g. `vboxmanage showvminfo win7-64`)
    - send keys: `controlvm <vm> keyboardputscancode <hex> [<hex>...]`
    - restore current snapshot: `snapshot restorecurrent`
    - run command: `guestcontrol <vm> run --exe <fullpath> --username <user> --passwordfile <pwfile> [-- arg0 arg1 ...]`


### Disks

- list physical partitions: `VBoxManage internalcommands listpartitions -rawdisk /dev/sda` (or `\\.\PhysicalDrive0`)
- mount physical disk / selected partitions in VM by creating a vmdk wrapper:
    - `vboxmanage internalcommands createrawvmdk -filename disk.vmdk -rawdisk /dev/...`
    - to select one or more partitions only: `-partitions 1,3,...`

    
## Misc

- mount shared folder (needs reboot after installing `virtualbox-guest-*`:
    - `sudo mount -t vboxsf -o uid=$UID,gid=$(id -g) share ~/host`
    - @ `fstab`: `name /path vboxsf uid=1000,gid=1000,noauto,rw 0 0`
- start VM headless: `VBoxManage startvm ubuntu-server --type headless`
    - or: `vboxheadless -s <vmname>`
        - enable VRDP: `-von`
    - enable VRDP permanently for a VM: `VBoxManage modifyvm "VM name" --vrde on` (requires the ext. pack)
- one-liner to stop, revert to snapshot & start a VM (headless):
    `vm=vmnane; vboxmanage controlvm $vm poweroff; vboxmanage snapshot $vm restorecurrent; vboxheadless -s $vm`


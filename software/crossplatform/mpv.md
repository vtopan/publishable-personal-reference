# mpv


## Install

- Windows: <https://mpv.srsfckn.biz/> or <https://sourceforge.net/projects/mpv-player-windows/files/64bit/>


## Shortcuts

- toggle always on top: `T` (`--ontop`)
- seek back / forward:
    - `Left` / `Right`: 5s (+Shift => 1s)
    - `Up` / `Down`: 1min (+Shift => 5s)
    - `Ctrl+Left / Right`: prev / next subtitle
    - `.` / `,`: by frame
    - `Space`: pause / resume (also: `right-click`)
- playback speed: `[` / `]` by 10%, `{` / `}`: 2x, `Bksp`: reset
- seek in playlist: `<` / `>`
- dec / inc volume: `/` / `*` (or `9` / `0`)
- fullscreen: `f`
- brief playback status: `o` (or `P`)
- subtitle sync: `x` / `z`
- subtitle up / down: `r` / `t`
- screenshot: `s` (w/o subtitles: `S`; as shown (scaled): `Ctrl+s`)
- image:
    - contrast: `1` / `2`
    - brightness: `3` / `4`
    - gamma: `5` / `6`
    - saturation: `7` / `8`
    - size: `Alt+0` / `Alt+2` (reset: `Alt+1`)
- quit saving position: `Q`


## Settings
 
- config file: `mpv.conf` in:
    - Linux: `~/.config/mpv/`
    - Windows: `%APPDATA%/mpv/` (or `portable_config/` next to `mpv.exe`)
        - `%APPDATA%` is usually `C:\users\<user>\AppData\Roaming`
    - example:

        ```
        ontop
        screenshot-directory=.
        save-position-on-quit
        ```

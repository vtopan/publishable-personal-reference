# nginx


## Commands

- check config: `sudo nginx -t`
- reload config: `service nginx reload`


## Setup


### Set up a site

- install: `apt install nginx`
- create site config (any filename) in `/etc/nginx/sites-available/`, softlink it in `sites-enabled/`
- config structure:
    - comments: `# ...`
    - `server { ... }`
    - directives:
        - port: `listen 443;`
        - server name/IP: `server_name server.local;`
        - `charset utf-8;`
        - root on disk: `root /var/www;`
        - index files: `index index.html index.htm;`
        - SSL:
            - `ssl on;`
            - certificate: `ssl_certificate /etc/server.crt;`
            - private key: `ssl_certificate_key /etc/server.key;`
        - basic auth:
            - message: `auth_basic "auth required";`
            - users: `auth_basic_user_file /etc/server.auth;` (`.htpwasswd` format: `user:pass[:comment]`)
        - pass requests to uWSGI (reverse proxy):
            - `include uwsgi_params;`
            - `uwsgi_pass unix:/run/uwsgi/app/<appname>/socket;` (or `localhost:54321`)
        - logging:
            - errors: `error_log /var/log/nginx/error.log [info|notice];`
        - per location settings: `location ^~ /static/ { root /var/static/; }`
- generate certificate + key:

    ```
    subj=/C=<2-letter-country-code>/ST=<state>/L=<location-city>/O=<company>/OU=<org-unit>/CN=<name>
    sudo openssl req -x509 -newkey rsa:4096 -nodes -subj "$subj" -out server.crt -keyout server.key
    ```

- generate basic auth:

    ```
    user=a; pass=a; echo "$user:$(expect -c 'set timeout -1;spawn openssl passwd -apr1;
        expect Password: {send ''$pass\n''}; expect Verifying {send ''$pass\n''};
        expect ''\$'' '|tail -n1)" > .htpasswd
    ```

- start / stop: `service nginx start` / `stop`


### nginx + php7.0-fpm

- install: `php php7.0-fpm nginx`
- ensure service is running (`php7.0-fpm`) and socket is listening (`netstat -plan|grep php` =>
  `/run/php/php7.0-fpm.sock`)
- `/etc/nginx/sites-available/test-php`:

    ``` 
    server {
        listen 8080 default_server;
        server_name local localhost;
        access_log   /var/log/nginx/test-php.access.log;
        error_log    /var/log/nginx/test-php.error.log;

        root /var/www/php;
        index index.php;

        location / {
            try_files $uri $uri/ /index.php?$args; 
        }

        location ~ \.php$ {
            root /var/www/php;
            include fastcgi_params;
            include snippets/fastcgi-php.conf;
            #fastcgi_pass 127.0.0.1:9000;
            fastcgi_pass unix:/run/php/php7.0-fpm.sock;
        }
    }
    ```

- enable server/site: `sudo ln -s /etc/nginx/sites-{available,enabled}/test-php`
- configure PHP: `sudo vim /etc/php/7.0/fpm/php.ini`
- might need to disable the default nginx server (`rm sites-enabled/default`) 
- reference:
    - <https://easyengine.io/wordpress-nginx/tutorials/single-site/minimal/>
    - <https://www.digitalocean.com/community/tutorials/how-to-install-linux-nginx-mysql-php-lemp-stack-on-ubuntu-14-04>
    - <https://www.globo.tech/learning-center/setup-nginx-php-fpm-mysql-ubuntu-16/>

# Radare2


## Cheatsheet


### Command line

| Parameter | Effect                             |
|-----------|------------------------------------|
| -w        | open file in RW mode (default: RO) |
| -q        | exit after processing commands     |
| -i script | run a script                       |
| -A / -AA  | analyze on load / extra analysis   |
| -c'...'   | run commands                       |
| -p        | create a project                   |


### Commands

| Command      | Effect                                                               |
|--------------|----------------------------------------------------------------------|
| ? <expr>     | eval expression (e.g. arithmetic)                                    |
| pdf          | print disassembly of function                                        |
| i            | basic info                                                           |
| ia           | all info                                                             |
| f            | print flags                                                          |
| fs #         | select flags category # (strings, imports, sections, ...)            |
| S            | section list                                                         |
| s...         | seek (e.g. `sS .text`)                                               |
| x            | print hex dump (alias of `px`)                                       |
| p...         | print (s = string, b = bits, D = disassemble, df = disasm. function) |
| /...         | search                                                               |
| aaa          | analyze all (same as `-A` on command line)                           |
| afl          | list functions                                                       |
| afn old new  | rename function                                                      |
| agv          | start web interface                                                  |
| !...         | run shell command                                                    |
| .command     | run command and define symbols from output                           |
| ..           | repeat command                                                       |
| ... ; ...    | join multiple commands on line                                       |
| afn old new  | rename functions                                                     |
| afvn old new | rename local vars / function args                                    |
| f-name       | delete flag by name                                                  |


- can pipe into `less` (e.g. `pdf|less`)
- `\`...\`` runs radare commands
- `~` = grep (`~!` = `grep -v`), e.g. `pdf~lgdt`


#### Search

| Subcommand | Effect                        |
|------------|-------------------------------|
| / ...      | find string                   |
| /w ...     | find WCHAR string             |
| /!_        | find first not matching       |
| /i ...     | ignore case                   |
| /e /rx/i   | match regex *rx*              |
| /x ...     | find hex string               |
| /c ...     | find code (ASM string)        |
| /a ...     | assemble and search for bytes |
| /A         | search for AES keys           |
| /z min max | search for string of length   |
| //         | repeat search                 |


### Visual mode

| Key           | Effect                                   |
|---------------|------------------------------------------|
| f             | define label                             |
| h / j / k / l | move (hold SHIFT to move faster)         |
| p / P         | change print mode                        |
| c             | toggle cursor                            |
| g / G         | seek to beginning / end of file          |
| d[f?]         | define function, data, code, ...         |
| x             | list crossrefs to jump                   |
| s / S         | step into / over                         |
| t             | trace flags (strings, functions etc.)    |
| T             | analysis info (sections, ...) + comments |
| v             | visual code analysis                     |
| u / U         | undo / redo seek (back / forward)        |
| y / Y         | copy / paste                             |
| mL / 'L       | set / jump to mark (L = letter)          |
| ; / -         | add / remove comment                     |
| .             | jump to PC (entrypoint if not debugging) |
| z             | toggle zoom mode                         |
| q             | back to shell                            |
| d[cd]         | define code / data                       |


### Configuration

- put commands in `~/.radare2rc` to have them run on load
- set config: `e ...`

| Option             | Effect                                                                    |
|--------------------|---------------------------------------------------------------------------|
| asm.cmtright=true  | Show comments at right of disassembly if they fit in screen               |
| asm.pseudo=true    | Show pseudocode in disassembly                                            |
| cmd.stack = true   | Display stack and register values on top of disasembly view (visual mode) |
| eco solarized      | Solarized theme                                                           |
| scr.utf8 = true    | Use UTF-8 glyphs                                                          |
| scr.truecolor=true | True color (24 bit) screen                                                |
| scr.rgbcolor=true  | 256 color screen                                                          |
| scr.pipecolor=true |                                                                           |
| scr.pager=less -R  | Better scrolling                                                          |


## Reference

- <https://github.com/radare/radare2/blob/master/doc/intro.md>
- [reverser.ninja CheatSheet](https://reverser.ninja/2015/23/radare2-cheat-sheet.html)
- <https://gist.github.com/pmauduit/3a81d409e2975fa546f5>
- Phrack: <http://phrack.org/issues/66/14.html#article>


## Examples

- disassembly: `pdf@sym.main`
- jump: `s entry0`
- command line:
    - open CLASS in JAR: `r2 zip://foor.jar//org/bar.class`
    - work on empty buffer: `r2 -` (512 bytes, use e.g `r2 malloc://4096` to get more)

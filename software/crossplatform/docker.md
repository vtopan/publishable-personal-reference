# Docker


## Dockerfile example

- Docker with an SSH server and ngrok:

```dockerfile
FROM ubuntu:latest
MAINTAINER io
ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get update && apt-get install -y openssh-server curl
RUN curl -s https://ngrok-agent.s3.amazonaws.com/ngrok.asc | \
  tee /etc/apt/trusted.gpg.d/ngrok.asc >/dev/null && \
  echo "deb https://ngrok-agent.s3.amazonaws.com buster main" | \
  tee /etc/apt/sources.list.d/ngrok.list && \
  apt update && apt install ngrok
RUN sed -ri 's/#?PermitRootLogin prohibit-password/PermitRootLogin yes/' /etc/ssh/sshd_config
RUN mkdir -p /root/{.ssh,.config/ngrok}
COPY id_rsa.pub /root/.ssh/authorized_keys
COPY ngrok.yml /root/.config/ngrok/ngrok.yml
RUN chmod -R 600 /root/.ssh
ENTRYPOINT service ssh start && bash
```

## Makefile template

```Makefile
IMAGE=<name>:<ver>
CONTAINER=<name>

build:
        sudo docker build -t "${IMAGE}" .

run:
        sudo docker start ${CONTAINER} || sudo docker run -it --name ${CONTAINER} ${IMAGE}

shell:
        sudo docker exec -it ${CONTAINER} bash 

stop:
        sudo docker stop ${CONTAINER}

rm-container: stop
        sudo docker rm ${CONTAINER}

rm-image: rm-container
        sudo docker image rm -f ${IMAGE}

status:
        sudo docker ps -af name=${CONTAINER}
```

# Firefox


## Privacy & security settings

- in about:config:
    - `dom.event.clipboardevents.enabled` = 0
    - `datareporting.healthreport.uploadEnabled` = 0
    - `browser.startup.homepage` = -
    - `browser.safebrowsing.*.enabled` = 0
    - `geo.enabled` = 0
    - `network.http.sendRefererHeader` = 0
    - `network.http.sendSecureXSiteReferrer` = 0
    - look for: "http:", "https:", "google", "upload", "uri", "telemetry"
    - questionable:
        - `dom.storage.enabled`
        - `network.dns.disablePrefetch`
        - `network.prefetch-next`


## Misc settings

- `dom.event.clipboardevents.enabled` = `false` will block JS from overriding clipboard events (e.g. blocking paste)

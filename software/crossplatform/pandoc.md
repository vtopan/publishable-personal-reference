# Pandoc


## Usage

`pandoc [sources...] [args] -o [output]`

- paramterers:
    - `-f ###`: input format
    - `-t ###`: output format (e.g. `-t dzslides` (HTML) or `-t beamer` (PDF) for slides)
    - `-s`: standalone (HTML)
    - `-c ###.css`: use CSS file
    - `-A ###`: append footer from file ###
    - `-V ###` / `--variable ###`: set variable
    - for PDFs:
        - `-V mainfont="..."`
        - `-V sansfont="Helvetica"`
        - `-V monofont="Courier New"`
        - `-V fontsize=12pt`
        - `-V version=1.2.3`
    - highlight code: `--highlight-style pygments` (or `kate`, `monochrome`, `espresso`, `haddock`, `tango`, `zenburn`)

- YAML header (first thing in the file):

    ```
    ---
    title: ...
    author: Joe Black
    date: June, 1984
    revision: 1
    ---
    ```


## Setup

- PDF output (Xetex/xelatex): `sudo apt-get install texlive-xetex -y`


## Misc

- enable plugins: add `+plugin_name` to format (e.g. `-f markdown+shortcut_reference_links`)
- ignore HTML tags in PDF: `-f markdown-markdown_in_html_blocks`
- page format: `-V geometry:paperwidth=210mm -V geometry:paperheight=297mm -V geometry:margin=2cm`
    - alternatives: the YAML header: `geometry: margin=3cm` or papersize directly: `-V geometry:a4paper`
- PDF output: `--latex-engine=xelatex`
- image size: `![image](img.png){ width=20px height=1cm }`
- make images inline: append `\`, i.e. `![...](...)\`
    - better - anchor all figures in the output PDF to the text location:

        ```latex
        \usepackage{float}
        \let\OrigFigure\figure
        \def\figure{\OrigFigure[H]}
        ```

        - reference: <https://en.wikibooks.org/wiki/LaTeX/Floats,_Figures_and_Captions>


## Troubleshooting

- problem: documents which contain JPG images cannot be converted to PDF - error "Dimension too large"
    - cause: the JPG doesn't contain proper EXIF DPI information
        - check with:
            - `exiftool img.jpg | grep Resolution` should say the resolution is "1"
            - `file img.jpg` output doesn't contain the word "DPI"
    - fix: add EXIF resolution either by re-encoding the image or by running
        `exiftool -jfif:Xresolution=72 -jfif:Yresolution=72 -jfif:ResolutionUnit=inches img.jpg`
    - reference:
        <https://tex.stackexchange.com/questions/243753/is-it-imagemagicks-fault-or-pdflatexs-that-some-jpegs-arent-working>


## Reference

- examples: <http://pandoc.org/demos.html>

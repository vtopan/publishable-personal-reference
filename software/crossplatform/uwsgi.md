# uWSGI


## Configuration

- .ini format:

    ```
    [uwsgi]

    # activate plugins
    plugins      = python3

    # vars
    project      = myproj
    base         = /my/proj/root

    # user / group
    uid          = www-data
    gid          = www-data

    # paths
    chdir        = %(base)/%(project)
    home         = %(base)/%(project)/venv_%(project)
    # either module (<file-without-py-ext>:<application-object-name>) or wsgi-file
    module       = wsgi:app
    #wsgi-file    = wsgi.py

    master       = true
    processes    = 5

    # default:
    #socket      = /run/wsgi/app/%(project)/socket
    #socket       = 127.0.0.1:54321
    #http         = 0.0.0.0:8080
    chmod-socket = 664
    vacuum       = true
    stats        = 127.0.0.1:9191
    ```


## Misc

- run directly: `uwsgi /path/to/ini --http localhost:8080`
- install service: `pip install uwsgi`


## Sample systemd unit

```
[Unit]
Description=uWSGI instance to serve myproject
After=network.target

[Service]
User=joe
Group=www-data
WorkingDirectory=/home/joe/myproject
Environment="PATH=/home/joe/myproject/myprojectenv/bin"
ExecStart=/home/joe/myproject/myprojectenv/bin/uwsgi --ini myproject.ini

[Install]
WantedBy=multi-user.target
```


## Reference

- <https://uwsgi-docs.readthedocs.io/en/latest/WSGIquickstart.html>
- <https://www.digitalocean.com/community/tutorials/how-to-serve-flask-applications-with-uwsgi-and-nginx-on-ubuntu-16-04>
- <https://www.digitalocean.com/community/tutorials/how-to-deploy-python-wsgi-applications-using-uwsgi-web-server-with-nginx>
- <https://www.nginx.com/blog/maximizing-python-performance-with-nginx-parti-web-serving-and-caching/>
- <https://uwsgi-docs.readthedocs.io/en/latest/Vars.html>

# ESXi


## Disks


### Creating a linked clone

- base can be thin provisioned
- power the VM off and create a snapshot
- retrieve the configuration (`.vmx`) and the snapshot disk descriptor (`...-000N.vmdk`)
    - in the descriptor, update the `parentFileNameHint` field to point to the full path of the base vmdk (`/vmfs/...`)
    - in the `.vmx`:
        - remove `sched.swap.derivedName`
        - update the `displayName`
        - adjust network configuration (at least MAC addresses) as needed, or remove the fields
            `ethernet0.generatedAddress` and `ethernet0.addressType`
        - remove UUIDs to avoid the "I copied it" prompt (`uuid.location` and `uuid.bios`)
- copy the snapshot disk extent (`...-000N-delta.vmdk`) and the modified config and disk descriptor to a new folder
- register the new `.vmx`
- on the first run answer the prompt with "I copied it" if UUIDs were not removed
- to freeze the parent, in its settings check Advanced > "Enable Template mode (to be used for cloning)"
    - missing setting in the web UI?
- references:
    - <http://sanbarrow.com/linkedcloneswithesxi>
    - <https://github.com/pddenhar/esxi-linked-clone/blob/master/clone.sh>


## Misc

- adding the SSH public key to authorized_keys: `/etc/ssh/keys-<username>/authorized_keys`
    - reference: <https://kb.vmware.com/selfservice/microsites/search.do?language=en_US&cmd=displayKC&externalId=1002866>
- synchronize VM time with host: VM Properties > Options > VMware Tools > "Synchronize guest time with host"


#### Enable SNMP

- SNMP v1/2c:

    ```
    esxcli system snmp set -r
    esxcli system snmp set -c MyCommunityString
    esxcli system snmp set -p 161
    esxcli system snmp set -L "Stockholm, Sweden"
    esxcli system snmp set -C noc@example.com
    esxcli system snmp set -e yes
    ```

- SNMP v3:

    ```
    esxcli system snmp set --engineid <id>
    esxcli system snmp set --authentication <protocol>
    esxcli system snmp set --privacy <protocol>
    ```

    The `id` must be a hexadecimal string between 10 and 32 characters long.
    The authentication `protocol` can be `none` (for no authentication), `SHA1`, or `MD5`. The privacy `protocol` can be
    `none` (for no privacy) or `AES128`.

    - add user: `esxcli system snmp set --users user1/08248c6eb8b333e75a29ca0af06b224faa7d22d6/232ba5cbe8c55b8f979455d3c9ca8b48812adb97/priv`
    - add passwordless user: `esxcli system snmp set --users user2/-/-/none`
    - query secrets: `esxcli system snmp hash --auth-hash <authashfile> --priv-hash <privhashfile>`
        - append --raw-secret to read from the command line



#### Change HTTPS vSphere port

- reference: <https://kb.vmware.com/selfservice/microsites/search.do?language=en_US&cmd=displayKC&externalId=1021199>
- get `proxy.xml`: `vifs --server <hostname> --username <username> --get /host/proxy.xml <local_directory_path>/proxy.xml`
- add `<httpPort>xx</httpPort><httpsPort>xxx</httpsPort>` in `<ConfigRoot>`
- put `proxy.xml` back: `vifs --server <hostname> --username <username> --put <local_directory_path>/proxy.xml /host/proxy.xml`
- use `Restart Management Agents` to restart `hostd`
- for the client, file `VpxClient.exe.config`: `<add key = "protocolports" value = "https:443;http:80:" />`

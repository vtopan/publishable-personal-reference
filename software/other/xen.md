# Xen


## xen-access

- requires `shadow_memory = 16` in DomU config to work


## altp2m support

- enable for guest: `altp2mhvm = 1` in DomU config


## Configuration


### Command line

- serial console:
    - HVM:
        - COM1: `kernel /xen-... com1=115200,8n1 console=com1,vga loglvl=all`
        - other com:
            - `dmesg | grep ttyS`
            - `com1=115200,8n1,0x3e8,5 console=com1,vga` (try other values if 1 doesn't work)
                - `0x3e8` is the IO port, `5` is the IRQ (try 0 or check dmesg)
    - PVM: `module /vmlinuz-... console=hvc0 earlyprintk=xen`
    - detach from console: Ctrl+]
    - client: `screen /dev/ttyUSB0 115200`

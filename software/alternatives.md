# Software Alternatives


## Libraries

- libc:
    - musl
    
    
## Sci

- matlab:
    - octave
    
    
## Project Management    

- https://taiga.io/
- https://hygger.io/
- https://gitlab.com/
- https://trac.edgewall.org/
- https://phacility.com/phabricator/
- https://en.wikipedia.org/wiki/Comparison_of_source_code_hosting_facilities

# Windows API


## Checking digital signatures

- most digsig APIs work exclusively with WCHAR strings
- enumerating entries in a digital signature catalog:
    - acquire provider: `CryptAcquireContext(&prov, NULL, NULL, PROV_RSA_FULL, 0))` (`HCRYPTPROV prov`);
    - open catalog file: `cat = CryptCATOpen(wszFile, 0, prov, 0x100, 0))` (`HANDLE cat`)
        - 0x100 == `CRYPTCAT_VERSION_1` (see also `CRYPTCAT_VERSION_2` on Windows 8+)
    - iterate members: `while ((ccm = CryptCATEnumerateMember(cat, ccm)))` (`CRYPTCATMEMBER *ccm`)
    - print `ccm->pwszFileName` (may be NULL!), `ccm->pwszReferenceTag`
    - close catalog: `CryptCATClose(cat)`
    - close provider: `CryptReleaseContext(prov, 0)`


## Misc

- get window station / desktop name from handle: `GetUserObjectInformation(..., UOI_NAME, ...)`


### Acquire privilege

    ```C
    LUID luid;
    DWORD size;
    TOKEN_PRIVILEGES tp;
    HANDLE htok = NULL;

    if (!LookupPrivilegeValue(NULL, SE_DEBUG_NAME, &luid))
    {
        LogError("LookupPrivilegeValue");
        return;
    }

    if (!OpenProcessToken(GetCurrentProcess(), TOKEN_ADJUST_PRIVILEGES, &htok))
    {
        LogError("OpenProcessToken");
        return;
    }

    tp.PrivilegeCount = 1;
    tp.Privileges[0].Luid = luid;
    tp.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED;

    if ((!AdjustTokenPrivileges(htok, FALSE, &tp, sizeof(TOKEN_PRIVILEGES), NULL, NULL)) || GetLastError())
    {
        // may fail even if it returns TRUE by setting ERROR_NOT_ALL_ASSIGNED
        LogError("AdjustTokenPrivileges");
    }

    if (htok)
    {
        CloseHandle(htok);
    }
    ```

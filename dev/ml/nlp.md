# Natural Language Processing (NLP)


## Python libraries

- [spaCy](https://spacy.io/) - spaCy is an open-source software library for advanced Natural Language Processing,
    written in the programming languages Python and Cython. It offers the fastest syntactic parser in the world.
    - Linux strongly preferred
    - [spaCy 101](https://spacy.io/usage/spacy-101)
    - [spaCy Universe](https://spacy.io/universe/) - additional libraries / related projects
        - [sense2vec](https://spacy.io/universe/?id=sense2vec)
        - [explacy](https://spacy.io/universe/?id=explacy)
        - [prodigy](https://spacy.io/universe/?id=prodigy) - Prodigy is an annotation tool so efficient that data
            scientists can do the annotation themselves, enabling a new level of rapid iteration.
        - [scattertext](https://spacy.io/universe/?id=scattertext)
        - [textacy](https://spacy.io/universe/?id=textacy)
    - [chat](https://gitter.im/explosion/spaCy)
- [pattern](https://github.com/clips/pattern)
    - "Web mining module for Python, with tools for scraping, natural language processing, machine learning, network
        analysis and visualization."
    - <https://www.clips.uantwerpen.be/pages/pattern>
- [gensim](https://radimrehurek.com/gensim/index.html)
    - [gensim tutorial](https://radimrehurek.com/gensim/tutorial.html)
- [tensorflow](https://www.tensorflow.org/install/pip) - An open source machine learning framework for everyone
    - use `tensorflow-gpu` if possible; Linux preferred
- [theano](http://deeplearning.net/software/theano/tutorial/examples.html) - Theano is a Python library that allows you
    to define, optimize, and evaluate mathematical expressions involving multi-dimensional arrays efficiently.
    - essentially a math (vector) library with deep learning features
    - [Blocks](https://blocks.readthedocs.io/en/latest/#quickstart) - Blocks is a framework that helps you build and
        manage neural network models on using Theano.
    - [Lasagne](https://lasagne.readthedocs.io/en/latest/) - Lasagne is a lightweight library to build and train neural
        networks in Theano.
- [Keras](https://keras.io/) - Keras is a high-level neural networks API, written in Python and capable of running on
    top of TensorFlow, CNTK, or Theano.


## Python oneliners / useful functions

- *gensim* utils:
    - `gensim.utils.deaccent(text)` - Remove letter accents from the given string.
    - `gensim.utils.decode_htmlentities(text)` - Decode all HTML entities in text that are encoded as hex, decimal or
        named entities.
    - `gensim.utils.lemmatize(content, ...)` - Use the English lemmatizer from *pattern* (external library) to extract
        lemmas.
    - `gensim.utils.simple_preprocess(doc, ...)` - Convert a document into a list of lowercase tokens.


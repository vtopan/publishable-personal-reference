# ML


## Learning


### Reinforcement Learning

- github/python samples:
    - [awesome list + Implementation of Reinforcement Learning Algorithms](https://github.com/dennybritz/reinforcement-learning)
    - [Using reinforcement learning to train an autonomous vehicle to avoid obstacles](https://github.com/harvitronix/reinforcement-learning-car)
    - [Theano-based implementation of Deep Q-learning](https://github.com/spragunr/deep_q_rl)
    - [Using Deep Q Networks to Learn Video Game Strategies](https://github.com/asrivat1/DeepLearningVideoGames)
- tutorials/papers:
    - [Simple Beginner’s guide to Reinforcement Learning & its implementation](https://www.analyticsvidhya.com/blog/2017/01/introduction-to-reinforcement-learning-implementation/)
    - [Reinforcement Learning - A Simple Python Example and a Step Closer to AI with Assisted Q-Learning](https://amunategui.github.io/reinforcement-learning/index.html)
    - [Learning Reinforcement Learning (with Code, Exercises and Solutions)](http://www.wildml.com/2016/10/learning-reinforcement-learning/)
    - [Simple Reinforcement Learning with Tensorflow Part 0: Q-Learning with Tables and Neural Network](https://medium.com/emergent-future/simple-reinforcement-learning-with-tensorflow-part-0-q-learning-with-tables-and-neural-networks-d195264329d0)
    - [Learning Gridworld with Q-learning](http://outlace.com/rlpart3.html)
    - [Using reinforcement learning in Python to teach a virtual car to avoid obstacles](https://blog.coast.ai/using-reinforcement-learning-in-python-to-teach-a-virtual-car-to-avoid-obstacles-6e782cc7d4c6)
- video courses:
    - [RL Course by David Silver - Lecture 1: Introduction to Reinforcement Learning](https://www.youtube.com/watch?v=2pWv7GOvuf0)
    - [6. Monte Carlo Simulation](https://www.youtube.com/watch?v=OgO1gpXSUzU)
    - [#3 Simplest Reinforcement Learning example (Eng python tutorial)](https://www.youtube.com/watch?v=gWNeMs1Fb8I)


## Reference

- Choosing the right estimator: https://scikit-learn.org/stable/tutorial/machine_learning_map/
- brief review: https://ml-cheatsheet.readthedocs.io/en/latest/
- https://github.com/afshinea/stanford-cs-229-machine-learning/blob/master/en/super-cheatsheet-machine-learning.pdf
    - https://stanford.edu/~shervine/teaching/cs-229/
- https://github.com/donnemartin/data-science-ipython-notebooks
- Supervised learning superstitions cheat sheet
    - https://github.com/rcompton/ml_cheat_sheet/blob/master/supervised_learning.ipynb


## Data

- Registry of Open Data on AWS: https://registry.opendata.aws/
- free trial (1TB / mo): Google Public Datasets @ https://cloud.google.com/bigquery/public-data/
- http://academictorrents.com/
- AI-specific: https://www.kaggle.com/datasets
- manually curated: https://dataturks.com/projects/trending
- GDELT: catalog of human societal-scale behavior and beliefs @ https://www.gdeltproject.org/
- NLP: https://machinelearningmastery.com/datasets-natural-language-processing/

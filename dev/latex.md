# Latex


## Math

- place math code between dollar signs: `$...$` (works in Pandoc Markdown as well)
- group / separate multiple symbols with `{...}`
- symbols: `\equiv` (identity), `\forall` (any), `\in` (belongs to), `\exists`, `\leq` (<=), `\epsilon`, etc.
    - multiplication dot: `\cdot`
    - 3 dots: `\dots`; 3 vert. dots: `\vdots`
- greek letters: `\alpha` / `\Alpha`, ...
- text position: `_...` (subscript), `^...` (superscript)
- fractions: `\frac{...}{...}`
- limits: `\lim_{...} ...`
- sums / products: `\sum_{...}^{...} ...` / `\prod_{...}^{...} ...`
    - use `\displaystyle\sum` to place the limits fully above / below sigma (may not be necessary / not work) or `\sum\limits`
    - sums over multiple variables: `\sum_{\substack{a=0 \\ b=0}}`
- integral: `\int_{...}^{...} ...` (double / triple / etc. integrals: `\iint` / `\iiint` / etc.)
- binomial: `\binom{...}{...}`
- n-order square root: `\sqrt[n]{...}`
- derivative: `\partial` for the partial derivative symbol
    - with the `physics` package:
        - derivative: `\dv{...}{...}`
        - partial derivative: `\pdv[order]{...}{...}` (`order` may be omitted)
- vectors: `\vec{...}` (arrow) or `\hat...` (`^`)
- matrix: `\begin{matrix} ... & ... & ... \\ ... \end{matrix}` (delimit cells with `\&` and rows with `\\`)
    - with brackets: `bmatrix`, round: `pmatrix`, curly: `Bmatrix`, straight: `vmatrix`, double: `Vmatrix`
    - diagonal matrix: only put in separators (e.g. `1&&&\\&1&&\\&&1&\\&&&1`)
- full-height parenthesis: precede with `\left` / `\right` (e.g. `\left(...\right)`)
- sets:
    - `\mathbb{R}` (real numbers) etc.
    - `\in` (∈)
- module: `\rvert` / `\lvert`; norm: `\rVert` / `\lVert` (use `\left` & `\right` prefixes to adjust height)
- reference: <https://en.wikibooks.org/wiki/LaTeX/Mathematics>
    - guess symbol online: <http://detexify.kirelabs.org/classify.html>


## Colors

- package: `\usepackage{xcolor}`
- defining colors: `\definecolor{colorname}{HTML}{FFF9E6}` (don't use lowercase hex digits!)
- reference: <https://en.wikibooks.org/wiki/LaTeX/Colors>


## Replace (redefine) tags

- e.g. set *texttt* background color:

    ```
    \let\OrigTextTt\texttt
    \renewcommand{\texttt}[1]{\OrigTextTt{\colorbox{hibgcolor}{#1}}}
    ```


## Misc

- line break: `\linebreak` (justify) or `\newline` (don't justify, only in paragraphs)


## Troubleshooting


### Defining HTML colors

- error: "Missing number, treated as zero." when defining a HTML color
- solution: don't use lowercase hex digits in HTML color codes

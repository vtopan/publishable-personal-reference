# Git


## Configuration


### Set user / password

- user: `git config --global user.name "YOUR NAME"` (skip `--global` to set for the current repo)
- email: `git config --global user.email "YOUR EMAIL ADDRESS"`
- password:
	- HTTPS:
        - `git config credential.helper store` (to save the password to `~/.git-credentials`)
	- git: use SSH key


### Set up a local git repo over SSH

```
sudo useradd -m git
# or:
# sudo mkdir /var/git # instead of -m
# sudo sed -r 's_/home/git_/var/git_' -i /etc/passwd
# sudo chown git:git /var/git
sudo passwd git # cat /dev/urandom|tr -cd "a-zA-Z0-9"|head -c20; echo
sudo su git
cd ~; mkdir .ssh && chmod 700 .ssh
# or, on another machine:
# ssh-copy-id git@$host
### add keys to >> ~/.ssh/authorized_keys
# make sure git-shell is a valid shell
gitshell=`which git-shell`
if ! grep -q git-shell /etc/shells; then echo $gitshell|sudo tee -a /etc/shells; fi; cat /etc/shells
# set git-shell for the git user
gituser=git
sudo sed -r "s~^($gituser:.+):[/a-z]*\$~\1:$gitshell~g" -i /etc/passwd; cat /etc/passwd
# add users to the git group (must log in again)
sudo usermod -a -G git $gituser
# chown the repo folder to the git user
sudo chown -R git:git /path/to/repo
```


### Add to auto-push to master

- on client:

    ```
    [remote "origin"]
        url = /mnt/usb/git/python3
        fetch = +refs/heads/*:refs/remotes/origin/*
    [branch "master"]
        remote = origin
            merge = refs/heads/master
    ```


- on server:

    ```
    [receive]
        denyCurrentBranch = ignore
    ```


### Sample .ssh/config

    ```
    Host sample
        User root
        Port 22
        HostName 10.1.1.10
        LocalForward 2222 10.1.1.11:22
        ForwardX11 yes
    ```


## Commands


### Generic

- partial staging: `git add -p`
- commit: `git commit -m "..."`
- revert: `git checkout -- filename`
- push changes to remote: `git push`
- pull changes from remote: `git pull`
- change branch: `git checkout branchname`
- create branch & change to it: `git checkout -b branchname`
- add stuff to commit: `git commit --ammend ...`
- list remote stuff: `git remote -v`
- brief status: `git status -s`
- set commit message editor: `git config --global core.editor "vim"`
- don't use less (pager): `git --no-pager ...`
- global: `git config --global core.pager ''`
- check staged diffs: `git diff --cached`
- allow pushes to local git repo: `git config receive.denyCurrentBranch ignore`
- rebase on pull: `git config --global pull.rebase true`
- clone submodule: `git submodule update --init libvmi`
- recover lost commits:
- search commit messages: `git log --all --grep='what-to-find'`
- commit:
    - signoff on commit: `-s`
    - edit message: `-e`
- generate patch:
    - if on a different branch: `git format-patch master --stdout > .patch`
    - if last 2 commits on master: `git format-patch @~2 --stdout > .patch`

    ```
    git reflog # => get commit id
    git merge a468c38
    ```


### Configuration

- file: `~/.gitconfig` (or per project `.git/config`)
- adding a command alias:

    ```
    [alias]
        find = log --all --pretty=\"format:%Cgreen%H %Cblue%s\n%b%Creset\" --name-status --grep
    ```

- user:

    ```
    [user]
        name = Foo Bar
        email = foobar@mail.com
    ```

- push strategy:

    ```
    [push]
        default = simple
    ```


### Misc

- create aliases: `git config --global alias.co "checkout"`
- show all branches: `git log --graph --all --oneline`
- change remote origin URL: `git remote set-url origin ssh://git@remote.host/project`
- change branch upstream: `git branch <branch> --set-upstream-to=<remote>/<branch>`
- move branch (rebase): `git rebase --onto <new-base-branch> <old-base-branch> <branch>`
    - e.g. `git rebase --onto master server client`: "Take the client branch, figure out the patches since it diverged
        from the server branch, and replay these patches in the client branch as if it was based directly off the
        master branch instead."
    - short version (might work): `git rebase --onto=<new-base> <branch>`
- fix pushed commit:

    ```bash
    git branch backup # save current state
    git rebase -i HEAD~3 # edit from 3rd commit in history
    # change pick->edit for interesting commits
    # make relevant changes to current state
    git commit -a --amend # commit *all* changes, amend last commit
    git rebase --continue
    git push --force # THIS RESETS THE REMOTE HEAD, might lose commits!
    ```

- ignore tag files globally:

    ```
    git config --global core.excludesfile '~/.cvsignore'
    echo tags >> ~/.cvsignore
    ```

- manage Mercurial repos with git: `wget https://raw.github.com/felipec/git-remote-hg/master/git-remote-hg -O ~/scripts/git-remote-hg && chmod +x $_`
- to create a "root" (origin) repo, clone or init with `--bare`
    - in the client repo use `git remote add origin <root-url>`
- force using SSH identity file for repo:

    ```
    Host github-custom
        HostName github.com
        User git
        IdentityFile /home/user/.ssh/github-custom
        IdentitiesOnly yes
    ```

- clone *bitbucket.org* repo via SSH with private key:
    - bitbucket.org username: `$BBUSER`, repo name: `$BBREPO`, path to key: `$SSHKEY`
    - command: `git clone ssh://git@bitbucket.org/$BBUSER/$BBREPO.git -c core.sshCommand="ssh -i $SSHKEY"`
    - with git for Windows, the path to the SSH key must be "posixized" (e.g. `c:\path\to\key` becomes `/c/path/to/key`)
- to use SSH for auth after cloning a public repo via HTTP(S), change the URL from `https://gitlab.com/<user>/<proj>` to `git@gitlab.com:<user>/<proj>` in `.git/config`
- debug SSH ops: `GIT_SSH_COMMAND="ssh -v" git push`


### Troubleshooting

- problem: `git: 'send-email' is not a git command. See 'git --help'.`
- solution: `apt-get install git-email`

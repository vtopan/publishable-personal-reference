# Sphinx

- Python (and C) documentation tool


## Code annotations / docstrings / comments

- to use a var comment as docstring, append a colon after the sharp: `MYVAR = 3   #: this is a docstring`
- parameters: `:param name: desc` (or specify type: `:param type name: desc`; add brackets to make it optional, e.g.
    `[type]`)
- return value: `:returns: desc`
- messages: `.. warning:: text`, `.. note:: text`
- refer doc from docstrings using the prefixes (roles): `:func:` (or `:py:func:`), `:py:mod:`, `:py:data:`,
    `:py:const:`, `:py:class:`, `:py:meth:`, `:py:attr:`, `:py:exc:`; e.g.

    ```
    This code calls :func:`fun1`.
    ```

    - for C code: `:c:func:`, `:c:member:` (structs/unions), `:c:macro:`, `:c:type:`, `:c:data:`
    - for JS code: `:js:mod:`, `:js:func:`, `:js:meth:`, `:js:class:`, `:js:data:`, `:js:attr:`
    - reference: <http://www.sphinx-doc.org/en/stable/domains.html>
- exceptions: `:raises ValueError: if ...`

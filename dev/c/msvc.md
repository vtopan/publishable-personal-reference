# MS Visual C++

- example: `cl file1.c file2.c file3.c /link /out:program1.exe`


## Environment variables

- compiler:
    - `%CL%` / `%CL\%` are prepended / appended to the compiler options (use `set _CL_=...` to set `%CL\%`)
    - `%INCLUDE%`: include search path
    - `%LIBPATH%`: metadata path (`#using ...`)
- linker:
    - `%LINK%` / `%LINK\%` are prepended / appended to the linker options
    - `%LIB%`: library search path
    - `%TMP%` (for e.g. .res building) and `%PATH%` (e.g. for `CVTRES`)
- reference:
    - compiler: <https://docs.microsoft.com/en-us/cpp/build/reference/cl-environment-variables>
    - linker: <https://docs.microsoft.com/en-us/cpp/build/reference/link-environment-variables>


## Print last error

```C
/**
 * Print the last Windows API error (as returned by GetLastError() to STDERR.
 * /
void
LogLastError(
    char *FunctionName
    )
{
    char *buf;
    DWORD lastError = GetLastError();

    FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
            NULL, lastError, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), (LPSTR)&buf, 0, NULL);

    printf("%s() failed <%08X>: %s", FunctionName, (int)lastError, buf);

    LocalFree(buf);
}
```

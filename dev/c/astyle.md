# astyle

- C/C++ formatting: "astyle --mode=c --style=bsd -s4 -xk -xl -C -K -w -U -p -H -j -c -xy -xC120 -z2 -k1"
    - `--mode=c`: C/C++ source code (other options: `java`, `cs` = C#)
    - `--style=bsd`: broken brackets (aka `allman` or `break`, same as `-A1`)
    - `-s4`: indent with 4 spaces
    - `-xk`: attach bracket to extern "C" (i.e. `extern "C" {\n..."`)
    - `-xl`: attach brackets to class inline function defs
    - `-C`: indent `class` blocks
    - `-K`: indent `case` blocks in `switch`es
    - `-w`: indent multiline `#define`s
    - `-U`: unpad (remove unnecessary padding around) parenthesis
    - `-p`: pad operators
    - `-H`: pad after paren headers (`if`, `for` etc.; e.g. `if (`)
    - `-j`: add brackets to one line conditional statements (e.g. `if (...)\n{\nstatement;\n}`)
    - `-c`: convert tabs to spaces
    - `-xy`: close ending angle brackets on template defs
    - `xC###`: set max line length at `###`
    - `-z2`: UNIX line endings (`\n`), `-z1`: Windows (`\r\n`)
    - `-k1`: align porinter operator (`*`, `&` or `^`) to the type
    - problems:
        - function parameters on subsequent lines are aligned to the first parameter


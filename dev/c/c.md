# C


## printf

- `%llu` for `unsigned long long` on Linux, `%I64u` on Windows/mingw


## Misc

- convert variable name to string macros: `#define STR_(x) #x` + `#define STR(x) STR_(x)`
- detect 32 vs 64 bits via preprocessor:
    - gcc/mingw: `#define BITS64 (__SIZEOF_POINTER__ == 8)`

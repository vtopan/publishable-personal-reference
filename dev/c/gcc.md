# gcc


## Parameters

- include debug information: `-g` (`-s` strips debug info during linking)
    - can specify level: `-g#` (2 = default; 1 = minimal information (e.g. no line numbers), 3 = full (e.g. macros))
- optimizations: `-O#`
    - `0` = off - default
    - `1` = reduce size & exec time with fast compile
    - `2` = same as `1` with compile time required
    - `3` = even more (e.g. inline functions)
    - `s` = optimize for size
    - `g` = optimize for debugging


## MingW


### No libc

```
CINCLUDE=/usr/i686-w64-mingw32/include
CCARG=-nostdlib -nodefaultlibs -lkernel32 -e _DllMain@12 -DNOLIBC
$(CC) -shared  $(SRC).c -o $(SRC).dll $(SRC).def -lshlwapi $(CCARG) -Wl,--image-base=0x66600000
```


### Cross-compiling for windows

- 32: `i686-w64-mingw32-gcc-win32`
- 64: `i686-w64-mingw32-gcc`


### Name decoration

- e.g. Main:
    - DLL (3 args): `_Main@12` @ 32 vs. `Main@12` @ 64
    - EXE (4 args): `_Main@16` @ 32 vs. `Main@16` @ 64


### Unicode (use wchar_t as default string type + call ...W APIs)

- `-municode` (this defines `UNICODE` and `_UNICODE`, switches from `main()` to `wmain()` and instructs the compiler /
    linker to use the appropriate libraries)


### Adding paths

- include path: `-Ic:/path/include`
- library path: `-Wl,-Lc:/path/lib64`
- object (.o) path: `-Bc:/path/lib64`

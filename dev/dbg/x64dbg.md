# x64dbg


## Plugins

- go in `x{32|64}/plugins`


### xAnalyzer

- <https://github.com/ThunderCls/xAnalyzer>
- the zip contents go in `apis_def` subfolders in both `plugins` folders


## Troubleshooting

- problem: xAnalyzer doesn't recognize functions properly
    - ensure the analyzed executable does not have dots in the filename

# WinDBG


## Command line parameters

- start kernel debugging session: `-k com:pipe,port=\\VMHost\pipe\PipeName[,resets=0]`


## Symbols

- symbol path: `srv*c:\symbols*https://msdl.microsoft.com/download/symbols`
- command: `.sympath`
- env var: `_NT_SYMBOL_PATH`
- verbose: `!sym noisy`
- reload: `.reload`


## Commands

- auto crash analysis (UM and KM): `!analyze -v`
- locks: `!locks`, `!qlocks`
- fix symbols: `!sym noisy; .symfix; .reload`
- set hardware breakpoint (on memory access): `ba [rwei]{size:1/2/4/8} addr` (e.g. `ba e1 0x6a0000`)
- structures: `!teb`, `!peb`
    - variables (pseudo-registers): `@$peb`
- get error info: `!error 2`
- breakpoint: `bp <location> ["what-to-do"]`
    - deferred: `bu <...>`


### Get information

- list all modules: `lm`
    - more detailed (UM): `!dlls`
- single module info: `lm vm ntdll` or `!lmi kernel32`
- thread info:
    - all threads: `~`; more details: `~*`; `!threads`
    - current thread (more details): `~.`
    - set current thread: `~<thread>s`
    - time consumed in threads: `!runaway`
- last error (UM and KM): `!gle`
- address info (process memory layout): `!address`
    - specific address / symbol: `!address <addr-or-name>`
    - protection info: `!vprot <addr>`
    - words and symbols: `dds`
    - stack regions: `!address -RegionUsageStack`
    - symbols near address: `ln ...` (use `/D` for DML output)
- stack:
    - current call stack: `k` / `kv`
    - find symbol on all stacks: `!findstack <symbol> 2`
    - all call stacks: `!uniqstack`
- find symbol: `x nt!*Process*`


### Dump / write data

- `ds` / `dS`: dump `ASCII_STRING` / `UNICODE_STRING`
- dump raw stack: `dd esp` / `dq rsp`
- dump known structure: `dt nt!_PEB <Addr>`
- dump module headers: `!dh kernel32`
- write data: `e* <address> <data>` (same pattern)


## Shortcuts

- restart process: `Ctrl+Shift+F5`


## Remote KD setup

- enable kernel debug messages:
  - dynamically: `ed nt!Kd_Default_Mask 8` (correct symbols must be loaded)
  - statically:
    - reg key: `HKLM\SYSTEM\CurrentControlSet\Control\Session Manager\Debug Print Filter`
    - reg value: `DEFAULT:REG_DWORD=0xFFFFFFFF`
      - use `IHVDRIVER` instead of `DEFAULT` for non-video/audio/network drivers
    - reference: <https://docs.microsoft.com/en-us/windows-hardware/drivers/debugger/reading-and-filtering-debugging-messages>


### VirtualBox

- in VirtualBox, VM Settings -> *Serial Ports*:
    - e.g. *COM4*
    - *Port Mode* "Host Pipe"
    - uncheck "Connect to existing..."
    - Path: `\\.\pipe\mykdpipe`
- in the VM:
    - allow unsigned drivers:
        - `bcdedit.exe /set testsigning on`
        - `bcdedit.exe /set loadoptions DISABLE_INTEGRITY_CHECKS`
    - enable kernel debugging: `bcdedit.exe /debug on`
    - configure: `bcdedit /dbgsettings serial debugport:1 baudrate:115200`


- on the host:
    - sympath: `SRV*c:\symbols*https://msdl.microsoft.com/download/symbols`
        - append `;SRV*c:\symbols*\\my.symbol.server\symbols` for custom symbol servers
        - append `;c:\mylocalsymbols` for local symbol folder
    - in WinDBG:
        - `Ctrl+K`, `COM` tab
            - check *Pipe*
            - *Port*: `\\.\pipe\mykdpipe`

            
## Misc

- downloading symbols manually:
    - URL: `https://msdl.microsoft.com/download/symbols/<filename>.pdb/<RSDS-GUID><RSDS-age>/<filename>.pdb`
        - get the RSDS GUID and age from the RSDS debug header (CodeView)
    - `User-Agent`: `Microsoft-Symbol-Server/10.1710.0.0`


## Reference

- download tools (EWDK): <https://docs.microsoft.com/en-us/legal/windows/hardware/enterprise-wdk-license-2019>
- <http://windbg.info/doc/1-common-cmds.html>
- <https://blogs.msdn.microsoft.com/willy-peter_schaub/2009/11/27/common-windbg-commands-reference/>
- <https://www.virusbulletin.com/uploads/pdf/conference_slides/2018/Svajcer-VB2018-KernelModeAnalysis.pdf>
- bcdedit:
  - <https://docs.microsoft.com/en-us/windows-hardware/drivers/devtest/bcdedit--debug>
  - <https://docs.microsoft.com/en-us/windows-hardware/drivers/devtest/bcdedit--dbgsettings>

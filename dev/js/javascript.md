# JavaScript


## Langauge


### for

```js
for (i = 0; i < cars.length; i++) {
    text += cars[i] + "<br>";
}
```

```js
var person = {fname:"John", lname:"Doe", age:25};

for (x in person) {
    // person[x] ...
}
```


## Patterns


### JSON async requests

- perform a request:

    ```js
    function do_it(query) {
        var req = new XMLHttpRequest();
        req.addEventListener("load", api_listener);
        req.addEventListener("error", api_failed);
        req.open("POST", '/api/op', true);
        req.setRequestHeader("Content-Type", "application/json");
        req.send(JSON.stringify(query));
        return false;
    ```

- get response: `function api_listener(evt) { ... this.responseText ... }`
- format JSON as HTML: <https://www.cssscript.com/minimal-json-data-formatter-jsonviewer/>
- in event handlers, the request object is in `evt.target`


## Misc

- print object attributes:

    ```js
    var v;
    for (var k in evt) {
        v = evt[k];
        document.getElementById("results").innerHTML += "evt[" + k + "]=" + v + "<br/>";
    }
    ```

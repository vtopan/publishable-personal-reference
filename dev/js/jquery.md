# jQuery (JavaScript)

- run on load: `$(document).ready(function(){...});`
- onclick: `$("button").click(function(){...});`
- set multiple attrs:

    ```js
    $("#w3s").attr({
        "href" : "http://www.w3schools.com/jquery",
        "title" : "W3Schools jQuery Tutorial"
    });    
    ```

- item contents: `.text("...")`, `.html("...")`, `.val("...")`
- `.each()`:

    ```js
    $( "li" ).each(function( index ) {
        console.log( index + ": " + $( this ).text() );
    });
    ```

- add content: `$(selector).append(content[, content])`, `$(selector).append(function)`

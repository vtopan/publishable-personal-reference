# Userscripts (JavaScript)


## Misc

- import jQuery: `// @require     http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js`
- use already-loaded jQuery: `var $ = unsafeWindow.jQuery;`


## Example


```js
// ==UserScript==
// @name        ...
// @namespace   xxx.com
// @include     http://some.site/*
// @version     0.1
// @grant       none
// ==/UserScript==

(function () {
  var $ = unsafeWindow.jQuery;
  
  ...
  
})();
```

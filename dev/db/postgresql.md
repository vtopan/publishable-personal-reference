# PostgreSQL


## Setup

- add user: `sudo -u postgres psql -c "CREATE ROLE <user> LOGIN NOSUPERUSER INHERIT CREATEDB [[ENCRYPTED] PASSWORD '<pass>'];"`
    - options:
        - `LOGIN` / `NOLOGIN`: allow / disallow login
        - `SUPERUSER` / `NO...`: superuser
        - `ENCRYPTED` / `UN...`: store password encrypted
        - `CREATEDB` / `NO...`: can create DBs
        - `CREATEROLE` / `NO...`: can create roles
    - reference: <https://www.postgresql.org/docs/current/static/sql-createrole.html>
- change/set password: `ALTER USER <user> PASSWORD '<pass>';`
- grant rights: `GRANT ALL ON DATABASE <db> TO <role>;`
    - reference: <https://www.postgresql.org/docs/current/static/sql-grant.html>


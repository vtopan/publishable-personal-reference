# Java


## Misc

- enable remote debugging: `java ... -agentlib:jdwp=transport=dt_socket,address=9999,server=y,suspend=n`


### Force JVM to use a proxy

- set env var `JAVA_TOOL_OPTIONS` to:
    - `-Djava.net.useSystemProxies=true`
    - or directly the proxy: `-Dhttp.proxyHost=1.2.3.4 -Dhttp.proxyPort=8800`
    - also: `-Dhttp.nonProxyHosts="localhost|127.0.0.1|10.*.*.*|*.foo.com‌​|etc"`

# Python

## requests

- log all requests (including redirects):

    ```
    def logging_hook(r, *a, **ka):
        print(r.url, r.status_code)
    session.hooks["response"] = [logging_hook]
    ```

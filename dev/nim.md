# nim

- tiny exe: `nimrod c --noMain --gc:none --compileOnly --os:standalone --genScript yourfile.nim`
    - ref: <https://forum.nim-lang.org/t/121>

## Troubleshooting

- mingw64 includes / libraries missing:
    - edit `config/nim.cfg`, add in an `@if windows:`:
        - `gcc.options.always = -Ic:/path/mingw64/lib64/gcc/x86_64-w64-mingw32/8.1.0/include"`
        - `gcc.options.linker = "-Bc:/path/mingw64/x86_64-w64-mingw32/lib64 -Bc:/path/mingw64/lib64/gcc/x86_64-w64-mingw32/8.1.0 -Wl,-Lc:/path/mingw64/x86_64-w64-mingw32/lib64`        

# Octave

- open-source variant of Matlab


## Ops

- arithmetic: `+` ..., `^` = exp., `==`, `~=`
- bool: `&&`, `||`, `xor(a, b)`
- `log()` (ln), `abs()`, `exp(x)` = e^x
- `min()`, `max()`, `floor()`, `ceil()`, `sum()`, `prod()`
    - element-wise max: `max(M1, M2)`
    - column/row-wise max: `max(M, [], dim)` (dim = 1/2)
    - (main) diagonal sum: `sum(sum(M .* eye(length(M))))`
- `find(cond)` (retrieves matching element indexes - can split in `[r,c]`)
- `mean(M[, direction])` (default direction: on columns = 1)
- `std(x)` - standard deviation
- get bit vector from number: `str2num(reshape(dec2bin(number, numbits)', [], 1))'`


## Variables

- list all variables: `who` / `whos` (more detailed)
- delete: `clear V` deletes `V`, `clear` deletes all variables


### Vectors

- range: `start:step:stop` (e.g. `1:0.1:10`)
- linear range (known count, infer step): `linspace(start, stop, step)`
- reverse: `fliplr(V)`


### Matrices

- init: `zeros(R[, C])`, `ones(R[, C])`, `eye(sz)` = identity matrix
    - random: `rand(R[, C])` (float 0..1), `randi([min max], R, C)` (int)
        - `randn(...)` - Gaussian (normal) distribution random
        - `magic(N)` - all rows/cols/diags sum up to the same value (int)
    - by replicating vector: `repmat(V, count, 1)` (or `(V, 1, count)` for a column vector)
        - e.g. raise a vector (`V`, #`n`) to a range of powers `1..p` to produce a matrix:
            - `repmat(V, p, 1) .^ repmat((1:p)', 1, n)`
- ops:
    - transpose: `'` (e.g. `x'`) or `transpose(x)`
    - inverse: `inv(x)` (slow)
        - use `pinv(x)` - works on singular matrices, provides approximate result
    - element-wise mul / div / exp: `.*`, `./`, `.^`
    - append column vector `V` to `M`: `M = [M V]`
    - generic matrix concatenation: `C = [A B]` (or `C = [A; B]`)
    - flip up-down: `flipud(M)` (e.g. on `eye()`)
    - convert from vector: `reshape(vec(start:end), rows, columns)`
- retrieve elements:
    - element at `r,c` from matrix `m`: `m(r,c)`
        - special position: `end` = last row / column
    - entire row `r`: `m(r,:)`; partial range: `a:b` (e.g. `m(1:3,4)`)
    - as single column (by concatenating columns): `m(:)`
- row/column count: `[dr, dc] = size(data)`
    - `size(M, dim)` returns the dim-th dimension of M
    - `length(M)`: largest of the dimensions (e.g. vector length)
- show as colored squares: `imagesc(M)`
    - grayscale + scale: `imagesc(M), colorbar, colormap gray`


## Graphs

- `plot(X)`, `plot(X, Y)`
    - `plot(x, y, 'r--o')` => red dashed line, mark points with 'o' (`k` = black, `s` = square, `:` = dotted, `-.` = dash-dot)
    - use `hold on` to keep adding to the same graph, `hold off` to move on
    - `title('...')`, `xlabel(...)` / `ylabel('...')`
    - `legend('...', ...)`
- `hist(...)` = histogram
- `scatter(X, Y)`
- subdivison: `subplot(R, C, elem)`
- axis ranges: `axis([m1 M1 m2 M2])`
- close: `close` (last figure), `close all`
- multiple graphs open: `figure(1)`, `plot(...)`, `figure(2)`, ...
- save: `print -dpng 'plot.png'`
- plot an inequality:

    ```
    [X,Y] = meshgrid(0:0.1:10,0:0.1:10);
    ineq = -3 + X > -Y;
    colors = zeros(size(X)) + ineq;
    scatter(X(:), Y(:), 3, colors(:), 'filled')
    ```


## Files & Folders

- save var. `v` to file `f.mat`: `save f v`; load with `load f`
- `print -dpng 'filename.png'` to print last graph
- `pwd`, `cd`, `ls`
- `addpath('...')` - add path to search path


## Flow control

- `if cond`, ...[`else`, ...], `end`; `for var in 1:n`, ..., `end`; `while cond`, ..., `end`
    - `break`, `continue` (also `while true ...`)

### Functions

- `function [out1, ...] = fun_name(in1, ...)`, ..., `end`
- no `return`, just set the `out1`, ... variables
    - brackets may be omitted when returning a single value
- can be defined anywhere in the script (incl. after use)
- can be saved to a file `<funname>.m` in the current folder to be used from the console
- anonymous (lambda): `f = @(x) ...` (=> `f` = "function handle" which can be passed as arg)
    - can use `@` ("handle") to pass any function as an arg


## Output

- print: `disp(...)`, `sprintf('...', ...)`
- `format long`: show more decimals
- the semicolon suppreses automatic output to console
- change prompt: `PS1('>> ')`


## Misc

- comments: `% ...`
- get index of e.g. `min()`: `[~, i] = min(a)`
- generate random points around y = x in Python:
    `y1=1;y0=1;n=100;import random;[random.randint(i - random.randint(5, 10), int(y0 + y1 * i + random.randint(5, 10))) for i in range(n)]`
- compact matrix display: `format compact`
- map array of numbers 1..10 to matrix containing 1 on the corresponding position in each row, zero otherwise:

    ```matlab
    f = @(x) (1:10)' == x       % map a number to a size-10 column with 1 on that position (e.g. 3 => [0 0 1 0 ...]')
    m = [3 5 10 2 ...]
    res = cell2mat(arrayfun(f, m, 'UniformOutput', false))'     % apply the function to m & convert cells to a matrix
    ```

# Windows Code Signing

## Signing files

- creating a certificate:
  - `makecert.exe -l https://google.com -e 12/31/2020 -r -pe -n "CN=..., O=..., OU=..., L=..., S=..., C=..." -ss My -sky exchange -sp "Microsoft Enhanced RSA and AES Cryptographic Provider" -sy 24 -sv certificate.pvk certificate.cer`
    - `-n <X509name>`: Certificate subject X500 name (eg: `CN=Fred Dews`)
    - `-pe`: Mark generated private key as exportable
    - `-ss <store>`: Subject's certificate store name that stores the output certificate
    - `-r`: Create a self signed certificate
    - `-e <mm/dd/yyyy>`: End of validity period
    - `-l <link>`: Link to the policy information
    - `-sky <keytype>`: Subject key type (`signature|exchange|<integer>`)
    - `-sy <type>`: Subject's CryptoAPI provider's type
    - `-sv <pvkFile>`: Subject's PVK file
  - => `certificate.cer`
- converting the certificate to .pfx:
  - `pvk2pfx -pvk filename.pvk -pi the-password -spc certificate.cer -pfx certificate.pfx -f`
    - `-f`: overwrite existing
- signing a file:
  - `signtool.exe sign /f certificate.pfx /t http://timestamp.digicert.com /p the-password filename-to-sign`
    - `-a <algorithm>`: The signature's digest algorithm (`md5|sha1|sha256|sha384|sha512`)
- checking signatures:
  - SysInternals sigcheck: <https://docs.microsoft.com/en-us/sysinternals/downloads/sigcheck>
  - more details: <https://blog.didierstevens.com/programs/authenticode-tools/>
  - using a catalog: `signtool verify /v /c catalog.cat filename.exe`
  - Linux:
    - `osslsigncode verify filename.exe`
    - or extract and dump with OpenSSL:
      - `osslsigncode extract-signature -in <infile> -out <certfile>`
      - strip the 8-byte WIN_CERTIFICATE: `dd if=<certfile> of=<certfile.p7b> bs=1 skip=8`
      - dump: `openssl pkcs7 -inform der -print_certs -in <certfile.p7b> -noout -text`


## Catalog files

- format: PKCS7 / DER
- dump:
  - `openssl asn1parse -in catalog.cat -inform DER`
- validate hash on file:
  - `signtool verify /v /c catalog.cat file.exe`
    - `/v` = verbose
    - check with explicit hash: `/hash SHA1|SHA256`
  - note: the SHA-256 used in catalogs (computed via `CryptCATAdminCalcHashFromFileHandle()`)
    does not match the actual SHA-256 hash of the files (at least for PEs)
- API reference: https://docs.microsoft.com/en-us/windows/win32/seccrypto/cryptography-functions#catalog-functions


## Reference

- https://docs.microsoft.com/en-us/windows-hardware/drivers/install/catalog-files
- signing a driver package: https://docs.microsoft.com/en-us/previous-versions/windows/it-pro/windows-server-2008-R2-and-2008/dd919238(v=ws.10)
- SignTool: <https://docs.microsoft.com/en-us/dotnet/framework/tools/signtool-exe>

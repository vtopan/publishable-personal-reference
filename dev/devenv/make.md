# Makefiles


## make parameters

- `-d`: full debug information
- `--debug[=b|v|i|j|m]`: selective debug (b: basic, v:verbose, i:implicit rules, j:command invocation, m:remaking)
- `-n`: just print commands which would be executed
- `-p`: print database on finish
- `-B`: unconditionally make all targets
- `-C dir`: cd to `dir`, then `make`
- `-j#`: run `#` jobs in parallel
- `-R`: no builtin variables
- `-s`: silent (don't echo commands)
- `-w` / `--no-print-directory`: force / disable printing when entering and leaving directories (recursive make)
- `-f file`: use `file` as Makefile


## Syntax

- var assignment: `V=...`, `V+=...` etc.


### Flow control

- `ifdef` / `elif` / `else` / `endif`
- check if file exists:

    ```
    ifneq (,$(wildcard file.ext))
        ...
    endif
    ```


### Pattern rules

- `%.o: %.c`
    - `e%t` matches the file name `src/eat` (the stem is `src/a`)
- implicit / pattern rule search algo for a target *t*:
    1. Split *t* into a directory part, called *d*, and the rest, called *n* (`src/foo.o` => d = `src/`, n = `foo.o`)
    2. Make a list of all the pattern rules which match d or n (pattern contains a slash => t, otherwise, n)
    3. Remove all nonterminal match-anything rules from the list if other kinds exist
    4. Remove from the list all rules with no commands.
    5. For each pattern rule in the list:
        a. Find the stem *s* (nonempty part of t or n matched by `%`)
        b. Compute the dependency names: *s* -> `%` (prepend *d* if the target pattern does not contain a slash)
        c. Test whether all the dependencies exist or ought to exist (explicit filenames)
        d. If all dependencies exist or ought to exist, or there are no dependencies, then this rule applies.
    6. If no pattern rule has been found so far, try harder. For each pattern rule in the list:
        a. If the rule is terminal, ignore it and go on to the next rule.
        b. Compute the dependency names as before.
        c. Test whether all the dependencies exist or ought to exist.
        d. For each dependency that does not exist, follow this algorithm recursively.
        e. If all dependencies exist, ought to exist, or can be made by implicit rules, then this rule applies.
    7. If no implicit rule applies, the rule for .DEFAULT, if any, applies. In that case, give t the same commands that
        .DEFAULT has. Otherwise, there are no commands for t.
- reference: <http://web.mit.edu/gnu/doc/html/make_10.html>


## Variables

- automatic:
    - `$^`: all prerequisites (dependencies)
    - `$<`: first prerequisite
    - `$@`: target name
    - `$*`: stem (what `%` is replaced by in a rule)
    - `$?`: all dependencies newer than the target
    - variations:
        - `$(<var>D)`: dir name (e.g. `$(@D)`
        - `$(<var>F)`: file (base) name
    - reference: <https://www.gnu.org/software/make/manual/html_node/Automatic-Variables.html>


## Functions


### Generic text

- replace:
    - with wildcards: `$(var:pattern=replacement)` <=> `$(patsubst pattern,replacement,text)`
        - e.g. `$(patsubst %.c,%.o,x.c)` => `x.o`
    - raw: `$(subst from,to,text)`
- strip whitespace: `$(strip string)`


### File names

- full path to current makefile: `PROJ:=$(dir $(abspath $(lastword $(MAKEFILE_LIST))))`
    - strip trailing slash: `PROJ:=$(PROJ:/=)`
- extension: `$(suffix names...)` => e.g. `.c .obj .exe`
- folder path (terminating `/` included): `$(dir ...)`
- file name (all but the path): `$(notdir ...)`
- add prefix / suffix: `$(addprefix/suffix suffix,names...)`
- absolute / full path:
    - `$(realpath paths...)` - path must exist, follows symlinks
    - `$(abspath paths...)` - path may not exist, does not follow symlinks
- join items from two lists: `$(join list1,list2)`
- glob: `$(wildcard pattern)`


## Misc


### bash

- EOLs need to be escaped (`\<LF>`), but ";" must also be passed


### Tricks & tips

- don't print "Entering directory [...]" messages: `--no-print-directory`
- list of all vars starting with a prefix, quoted: `DOCS=$(foreach V, $(filter DOCF_%, $(.VARIABLES)),$($V))`


## Examples

- basic pattern rule:

    ```
    %.o: %.c foo.h
            $(CC) -c $(CFLAGS) $(CPPFLAGS) -o $@ $<
    ```

- double target rule (a single command produces two files):

    ```
    %.tab.c %.tab.h: %.y
            bison -d $<
    ```

- get field (e.g. version / build number) from file: `REV:=$(shell grep -P '^revision:' base.c|cut -d: -f2|xargs)`
- assume path for a (source?) file extension: `vpath %.dia dia`
- all files with an ext: `SOURCES=$(shell ls src/*.c|sort)`
- add a prefix to a list of files (and change ext): `EXES=$(addprefix bin/,$(SOURCES:.c=.exe))`


## Reference

- GNU make manual: <https://www.gnu.org/software/make/manual/make.html>

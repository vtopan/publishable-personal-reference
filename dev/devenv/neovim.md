# Neovim


## Setup


### Windows

- download, unpack
- put all plugins in `<installl-path>\share\nvim\runtime\pack\bundle\start`
    - run `mklink /d plugins.link share\nvim\runtime\pack\bundle\start` in the program dir to have a quick jump point to the plugin folder
- `init.vim` goes into `%LOCALAPPDATA%\nvim` (usually `c:\Users\<user>\AppData\Local\nvim\init.vim`)


## Misc

- enable logging: `export NVIM_PYTHON_LOG_FILE=/tmp/log`, `export NVIM_PYTHON_LOG_LEVEL=DEBUG`



# Vim plugins


## Ctrl+P

- Full path fuzzy file, buffer, mru, tag, ... finder for Vim
- <https://github.com/ctrlpvim/ctrlp.vim>
- shortcuts:
    - `<C-p>` in normal mode to open files fuzzily
        - `<C-t>` in C-P view to open file in new tab
        - `<F5>` to purge cache
- config:
    - extra oot markers: `let g:ctrlp_root_markers = ['pom.xml', '.p4ignore']`
        - implicit/default: `.git, .hg, .svn, .bzr, _darcs`


## Dockerfile

- Vim syntax file for Docker's Dockerfile and snippets for snipMate
- <https://github.com/ekalinin/Dockerfile.vim>


## gen_docs.vim

- Async plugin for vim and neovim to ease the use of ctags/gtags
- <https://github.com/jsfaint/gen_tags.vim>
- commands: `:GenCtags`, `:ClearCtags`
- shortcuts:
    - default:
        - `<C+\>` + one of:
            - `c` Find functions calling this function
            - `d` Find functions called by this function
            - `e` Find this egrep pattern
            - `f` Find this file
            - `g` Find this definition
            - `i` Find files #including this file
            - `s` Find this C symbol
            - `t` Find this text string
    - custom:
        - `nnoremap GT :GenCtags<CR>`


## incsearch.vim

- incsearch.vim incrementally highlights ALL pattern matches unlike default 'incsearch'.
- <https://github.com/haya14busa/incsearch.vim>


## CamelCaseMotion

- motions:
    - `<A-motion>` stops inside words at CamelCase boundaries or at underscores
        - e.g. `di<A-w>`, `<A-b>` etc.
    - `<A-left/right>` in `Ins` mode jumps to CC/_ in-word boundaries


## Nerdtree

- toggle: `<leader>n`


## jedi-vim

- completion: `<C-Space>`
- go to assignment: `<leader>g`
- go to definition: `<leader>d`
- documentation (docstring) for word under cursor: `K`
- rename: `<leader>r`
- find references: `<leader>n`
- open module source: `:Pyimport ...`


## OverCommandLine

- `/` - search with highlight
- `<C-h>` - substitute with preview


## fzf

- commands (shortcut: `<leader>z[initial]`, e.g. `,zf`):
    - `:Files` - find in file names (live)
    - `:Rg` / `:Ag` - find in files (live)
    - `:Tags` - ctags tags
    - git:
        - `:GFiles` - git ls-files
        - `:Commits` - git commits
        - `:BCommits` - git commits for the current buffer
    - misc: `:Marks`, `:Colors`, `:Buffers`, `:History/`, `:Maps`, `:Helptags`, `:Filetypes`
- in ins mode: `<c-i>f` to insert filename from project


## emmet

- emmet leader: `,h`
    - expand snippet: `,`
    - select tag + contents: `d`
    - go to matching tag: `t`
    - contract / expand tag: `j`
- snippets: `html:5`, `html>head>title+meta`, etc.
    - cheatsheet here: <https://docs.emmet.io/cheatsheet-a5.pdf>


## vim-gitgutter

- (c)hunks:
    - navigate: `[c` / `]c`
    - stage (add): `<leader>hs`
        - undo staged hunk: `<leader>hu` (cannot unstage non-vim-staged hunks)
    - preview staged: `<leader>hp`
    - text objects (selectors): `ic` / `ac`
- toggle hunk line highlight: `<leader>ht` (mapped to `:GitGutterLineHighlightsToggle`)


# VSCode


## Shortcuts

- commands (interactive): `F1` or `Ctrl+Shift+P` 
- settings: `Ctrl+,`
- build: `Ctrl+Shift+B`
- open folder: `Ctrl+K Ctrl+O`


## tasks.json


### Reuse

- can move (some) common fields from multiple tasks to the parent dict
    - movable: `"options", "problemMatcher", "type", "presentation"`
    - non-movable: `"label" (duh), "group", "command", "args" (?)`
- hint: always set `"presentation": {"clear": true}` (clears console before running task)


### Sample

```JSON
{
    "version": "2.0.0",
    "tasks": [
        {
            "label": "build",
            "type": "shell",
            "windows": {
                "command": "${workspaceFolder}\\build.bat",
            },
            "args": [
                "/property:GenerateFullPaths=true",
                "/t:build",
                "/p:Configuration=Release",
                "/p:Platform=Win32"
            ],
            "options": {
                "cwd": "${workspaceRoot}",
            },
            "group": "build",
            "presentation": {
                "clear": true
            },
            "problemMatcher": "$msCompile"
        }
    ]
}
```

## Misc

### Hide files in file browser (e.g. __pycache__)

- in Settings (Ctrl+,) go to (type) files:exclude, add `**/__pycache__`

# vim


## Setup


### Plugins


- DO NOT INSTALL `YouCompleteMe`!


## Shortcuts

```
:res N/+N/-N    resize window (lines)
<C-w>+/-    resize window up/down
<C-w>>/<    resize window left/right
:set tw=120 hard line length
gqap        reflow paragraph
q<reg>...q  record macro in register <reg>
qQ...q      append to macro recorded in register q
@<reg>      execute macro in <reg>
* / #       find word under cursor forward / backward
g* / g#     find text under cursor forward / backward
:sp filename    open in horizontal split (vsp for vertical) / tab
gf          open file under cursor
ZZ / ZQ     quit saving / without saving
<C-V>       visual block mode
m<m> / `<m> / '<m>  set mark (a-z / A-Z) / go to mark / go to mark bol (lowercase = local)
.           repeat command
u / <C-r>   undo / redo
:!<cmd>     execute shell command
dd          delete line
gU / gu     change to uppercase / lowercase
x / X       delete char after / before cursor
<C-P>       local autocomplete
<C-n>       keyword completion
> / <       indent / unindent?
za          toggle folding
gt          go to file under cursor
K           go to man page of word under cursor
g<C-G>      get word count
```


### Mode

```
v       visual mode
<C-v>       visual mode - vertical blocks
a / A       append at cursor / at EOL
i / I       insert insert at cursor / before first non-blank
o / O       new line above / below cursor
s / S (cc)  erase char / line and enter insert mode
cw      delete word, enter insert mode
```


### Movement

```
w / W       current word / up to next word
s / p       sentence / paragraph
b / B       block (...) / {...}
i# / a#     inside / a # (# = w, s, b, ...)
i( / i" / it    inside parenthesis / quotes / HTML tags (finds the first one on the line if not currently
inside)
}           end of paragraph
f<c> / F<c> / t<c>  char <c> forward / backward / before char <c>
<C-o>       jump back
<C-i>       jump forward
'.          jump to last modified line
`. (or g;)  jump to last modified position
```


### Navigation

```
H / M / L   jump to top / middle / bottom of string
<C-b> / <C-f>   page up / down
w / e / b   jump to next start / end of word / back, punctuation included
W / E / B   jump to next start / end of word / back, punctuation ignored
zt / zz / zb    scroll cursor to top / middle / bottom
0 / ^ / $   jump to start / first non-blank / end of line
gg / G      go to top / bottom of file
#G / #gg    go to line #
`.          jump to last change in buffer
``          jump back
]' / ['     jump to next / prev local mark
```


#### Code-specific:

```
%           jump to matching #if etc.
[[ / ]]     prev / next function (looks for } at the beginning of the line)
[{ / ]}     jump by codeblock start / end
gd / <C-]>  jump to var definition
, / ;       repeat f/F forward / backward
[m          jump to start of function body
[i          jump to first appearance
[I          show all occurences
```


### Code folding

```
zf      manual fold (e.g. zfa} )
zr      expand all folds
zm      fold all (auto)
zfit    fold inner tag (HTML)
```


### Windows

```
<C-w>s / v  split in windows horizontally / vertically
<C-W>w      next window
<C-w><arrow>    jump to window up/down/...
<C-w>< / >  resize window left / right
<C-w>+ / -  resize window up / down
<C-w>=      resize to equal dimensions
```


### Buffers

```
:ls         list all buffers
:bdN        delete buffer N
```


### Location list

```
:lop        open
:lcl        close
:lne / :lpr next / prev item (can pass height as arg, e.g. `:lop 3`)
```


### Clipboard

```
y / yy      yank (copy) selection / line
"<reg>y     yank (copy) selection into register; <reg>=[a..z*]
:%y+        yank page
c       cut
p / P       paste after / before
"<reg>p     paste from register (+ = global clipboard)
```


### Ranges

```
start,end   lines from start to end
. / $       current / last line
%           all lines
.,.N        N lines from current
'x          mark x (whole line)
'<,'>       last visual selection (=> can use vip etc.)
/pat/ / ?pat?   first line after / before which matches pat
```


### Complex sequences

- `yypv$r-     underline with -`


### My shortcuts

```
<C-L>       redraw screen & clear highlights (default: just redraw)
`<sym>      symbols when romanian keyboard is active
<F12> / <C-F12> load / save session
\pdf        generate pdf from md
<F9>        run current file
<F2>        save
<C-h/j/k/l> jump to split
```

### Defining shortcuts

- key map:


    ```
    <BS>           Backspace
    <Tab>          Tab
    <CR>           Enter
    <Enter>        Enter
    <Return>       Enter
    <Esc>          Escape
    <Space>        Space
    <Up>           Up arrow
    <Down>         Down arrow
    <Left>         Left arrow
    <Right>        Right arrow
    <F1> - <F12>   Function keys 1 to 12
    #1, #2..#9,#0  Function keys F1 to F9, F10
    <Insert>       Insert
    <Del>          Delete
    <Home>         Home
    <End>          End
    <PageUp>       Page-Up
    <PageDown>     Page-Down
    <bar>          the '|' character, which otherwise needs to be escaped '\|'
    ```

    
## Commands


```
:e filename         open file in current buffer
:wa!                write (save) all force
:nohl               stop highlighting search results
:map                list key mappings
:verbose map        list key mappings and where they were defined
:source $MYVIMRC    reload config
:[range]sort        sort lines
:marks              list marks
:! cmd              run command ( `:.!` => run & paste in buffer)
:!%:p               run current file
:source $MYVIMRC    reload .vimrc
:s/pat1/pat2/g      substitute regex pat1 with pat2 on the current line (use :%s... to sub in the entire file)
:retab              apply tab settings
```

- save current session: `:mksession`
- load: `:source ~/mysession.vim`
- change CRLF to LF: `:setlocal ff=unix :w`


## Plugins

### Pymode plugin

- python help: `K`


### NERDCommenter

```
\cc     comment line
\c<space>   toggle comment line
\ci         toggle comment line individually
\c$         comment to EOL
\cA         append comment
\ca         alternative comment mode toggle
\cs         sexy comment (/* * *...*/)
```


### DrawItPlugin

- `\di start`
- `\ds stop`


### Vimball plugin

- install vimball:

```
vim plugin.vba
:so %
```


### Textformat plugin

- align text: `\a[clrj]` - align center/left/right/justify


### Tablemode plugin

- `\tm     :TableModeEnable    toggle table mode`
- `\tt     :Tableize`
- <https://github.com/dhruvasagar/vim-table-mode/>
- in insert mode: `|...|...` or `||`


### Tabularize plugin

- align at `:=|<tab><space>`: `\a{:=|t<space>}`


### Cscope & ctags (cscope_maps plugin)

- `Ctrl-\` key:
   - s - symbol: find all references to the token under cursor
   - g - global: find global definition(s) of the token under cursor
   - c - calls:  find all calls to the function name under cursor
   - t - text:   find all instances of the text under cursor
   - e - egrep:  egrep search for the word under cursor
   - f - file:   open the filename under cursor
   - i - includes: find files that include the filename under cursor
   - d - called: find functions that function under cursor calls
- jump to definition: `C-]`
- show list of defs: `g C-]`
- go back: `C-T`
- create DBs:

    ```
    find /path \( -name '*.[chsS]' -o -name '*.[ch]pp' \) -print > cscope.files
    ctags -L cscope.files
    cscope -b -q -k
    ```


Settings
--------

- syntax: `:set <setting>` (disable with `:set <setting>!`)

    ```
    ----------- ---------------------------------------------------------------------------------------------
    number      line numbers
    cc          text width marker
    textwidth   text width hard limit
    tabstop     tab size in spaces
    mouse       mouse mode (a => integration, nothing => let term handle mouse - proper selections & copy/paste)
    ----------- ---------------------------------------------------------------------------------------------
    ```
    
- auto-reload .vimrc on change: `autocmd BufWritePost .vimrc source %`
- remove trailing whitespace: `autocmd BufWritePre *.py :%s/\s\+$//e`
- expand tabs: `set tabstop=4 shiftwidth=4 expandtab :retab`
- global / system clipboard: `set clipboard=unnamed` or `unnamedplus`
    - note: might need vim with x support; install vim-gtk on ubuntu (check: vim --version | grep clipboard =>
      +clipboard or +xterm_clipboard)
- interpret extension as filetype: `au BufNewFile,BufRead *.myjson set filetype=json`
- use DOS line ends on batch files: `au BufNewFile,BufRead *.bat,*.cmd set ff=dos`


Line ranges
-----------

```
----------- --------------------------------------------------------------------------
number      absolute line number
.           the current line
$           the last line in the file
%           the whole file. The same as 1,$
't          position of mark "t"
/pattern[/] the next line where text "pattern" matches.
?pattern[?] the previous line where text "pattern" matches
\/          the next line where the previously used search pattern matches
\?          the previous line where the previously used search pattern matches
\&          the next line where the previously used substitute pattern matchesregex
----------- --------------------------------------------------------------------------
```

Each may be followed (several times) by "+" or "-" and an optional number. This
number is added or subtracted from the preceding line number. If the number is
omitted, 1 is used.

- examples:
    - `/Section 1/+,/Section 2/-` - all lines between Section 1 and Section 2, non-inclusively, i.e. the
    lines containing Section 1 and Section 2 will not be affected.
    - `:/Section/+ y` will search for the Section line and yank (copy) one line after into the memory
    - `:// normal p` will search for the next Section line and put (paste) the saved text on the next line
    - `/Section 1/;/Subsection/-,/Subsection/+` - first find Section 1, then the first line with Subsection,
    step one line down (beginning of the range) and find the next line with Subsection, step one line up (end
    of the range)


## vim regex

- *substitution*: ``:range s[ubstitute]/pattern/string/cgiI``
    - c: confirm
    - i/I: ignore/don't ignore case
    - e.g.: `s:\s*$::` - replace eol blanks
- ops/chars which work unescaped: `. * } [] & ~`
    - pay attention to `\( \) \{ \+ \|`
- non-PCRE:
    - `\=` <=> `?`
    - `\{-}` <=> `*?`
    - `\{-m,n}` non-greedy `{m,n}`
    - backreferences:
        - `&` whole match (`\0` works)
        - `~` previous substitue string
        - `\L` / `\U` - lower/uppercase prefix for following chars (end with `\e` or `\E`)
        - `\r` - proper end of line
        - `\l` / `\u` - next char lower/upper
- *global command*: `:range g[lobal][!]/pattern/cmd`
    - e.g.: `:10,20g/^/ mo 10` - sort lines 10-20


## Modelines

- setup:

    ```
    set modeline
    set modelines=2
    ```

- uses:
  - `# vim:syn=sh`
  - `# vim:fileencoding=utf-8` (`fileencoding` == `fenc` controls what is writen to files, `encoding` == `enc` controls
  what is displayed (default taken from `$LANG`)
  - `# vim:fileencoding=utf-8:syn=python` (filetype & encoding for Python scripts)
  - `rem vim:ff=dos` (line ends for Windows .bat scripts)
- reference: <http://vim.wikia.com/wiki/Modeline_magic>


## Misc

- change EOLs DOS<->UNIX:

    ```
    :update
    :e ++ff=dos
    :setlocal ff=unix
    :w
    ```

- find TODO/FIXMEs & add them to the quickfix list:
    `:g/TODO\|FIXME/cexpr expand("%") . ":" . line(".") .  ":" . getline(".")`


## Reference


### Cheat sheets

- <http://vim.rtorr.com/>
- <http://keyxl.com/aaa8263/290/VIM-keyboard-shortcuts.htm>
- <http://www.viemu.com/vi-vim-cheat-sheet.gif>


## Troubleshooting

### Vim doesn't copy to global clipboard no matter what

- may not be built with `+xterm_clipboard`; check with `vim --version|grep clip`
- the vim in the "vim" package on Ubuntu-server isn't; use `apt-get install -y vim-gui-common` to get the
  proper one
- use the "unnamedplus" clipboard: in .vimrc `set clipboard=unnamedplus`
- [findme] xclip


### Vim complains about invalid values in .vimrc (e.g. listchars)

- UTF-8 characters in `.vimrc` and `LC_ALL` set to something that can't interpret them
- set `LC_ALL` to `en_US.UTF-8` or `apt-get install debconf; dpkg-reconfigure locales`


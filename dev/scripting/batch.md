# Windows Batch Files (BAT / CMD)


## Registry

- *reg add*: `reg add "HKLM\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Windows" /v AppInit_DLLs /d spy.dll` 
    - `/f` - force (no confirmation for e.g. overwrite)
    - `/t DataType`: `REG_SZ` (default) / `REG_DWORD` / `REG_EXPAND_SZ` / `REG_MULTI_SZ`
    - choose registry: `/reg:32` or `/reg:64` (`HKLM\Software` vs. `HKLM\Software\Wow6432Node`)
- reference: <https://ss64.com/nt/reg.html>


## Mix batch & JS in the same script file

```bat
@if (@CodeSection == @Batch) @then

@echo off

set SendKeys=CScript //nologo //E:JScript "%~F0"
ping -n 5 -w 1 127.0.0.1
start .\cc6.exe
%SendKeys% "{ENTER}"

goto :EOF

@end

// JScript section

var WshShell = WScript.CreateObject("WScript.Shell");
WshShell.SendKeys(WScript.Arguments(0));
```

```bat
@if (@X)==(@Y) @goto :Dummy @end/* Batch part

@echo off
SETLOCAL ENABLEEXTENSIONS
for /f "delims=" %%x in ('cscript //E:JScript //nologo "%~f0"') do set desk=%%x
echo desktop path is %desk%
goto :EOF

***** Now JScript begins *****/
WScript.Echo(WScript.CreateObject("WScript.Shell").SpecialFolders("Desktop"));
```

- alternative hybrid prefix:

    ```bat
    @set @foo=(
    @goto ^@BATCH )
    ```

- reference:
    - <https://stackoverflow.com/questions/21693233/conditional-compilation-turned-off#21694562>
    - <https://stackoverflow.com/questions/2591310/is-there-a-short-cut-for-desktop-folder-in-windows-batch/5656250#5656250>


## Unattended runas (provide password from batch file)

```bat
@if (@CodeSection == @Batch) @then
@echo off
start "" runas /user:testuser c:/path/to/my/program.exe
CScript //nologo //E:JScript "%~F0"
goto :EOF
@end
WScript.CreateObject("WScript.Shell").SendKeys("password{ENTER}");
```


## Misc

- wait for # seconds: `choice /N /T 10 /D Y`
    - the `timeout` command doesn't work with redirected output
    - the `waitfor` command only exists on Windows 7+
    - could also use `ping` with `-w`, but a guaranteed non-reachable address is required
- redirect stderr to stdout: `>... 2>&1` (the order is important)
- redirect the output of the current script:

    ```
    @echo off

    set self=%0
    set out=output.txt
    call :redirect >%out% 2>&1
    type %out%
    goto end

    :redirect
    echo Doing the stuff as %self% @ %date%, %time%...

    :end
    ```

    - remove the "type" line to skip printing the output to screen as well

- convert Unicode (UTF-16) output to ASCII: pipe through `more` (appends an extra newline)
- pipe through `findstr .` to filter empty lines
- put the output of a command in a variable: `for /f "delims=" %%a in ('ver') do @set foobar=%%a`
    - escape special symbols (`|`, `&`, etc.) with `^`
- get date & time in separate variables:

    ```
    :: Use WMIC to retrieve date and time
    FOR /F "skip=1 tokens=1-6" %%A IN ('WMIC Path Win32_LocalTime Get Day^,Hour^,Minute^,Month^,Second^,Year /Format:table') DO (
    IF NOT "%%~F"=="" (
    :: fixed order of output: day, hour, minute, month, second, year (alphabetic)
        SET day=%%A
        SET hour=%%B
        SET minute=%%C
        SET month=%%D
        SET second=%%E
        SET year=%%F
        )
    )
    ```

- get substring from variable: `%VAR:~<start>[,<count>]%`
- send keys to top level app: use WSH (`WScript.CreateObject("WScript.Shell").WshShell.SendKeys("{ENTER}");`)
- check credentials by mapping C$:

    ```
    net use \\localhost\C$ %password% /user:%user%
    net use /delete \\localhost\C$
    ```

    - reference: <https://stackoverflow.com/questions/42959621/command-batch-credential-authentication-not-working>

- run `git pull` in all subfolders: `for /d %i in (*) do cd %i && git pull & cd ..`
- enable command extensions: `setlocal enableextensions`
- `pause` if not interactive: `echo %cmdcmdline%|find /i "%~0">nul&&pause`
- delayed expansion: `setlocal enabledelayedexpansion`
- iterate drive type (3=hdd, 5=cdrom) letter and name:

    ```bat
    setlocal enabledelayedexpansion
    for /f "skip=1 tokens=1,2,3 delims=: " %%a in ('wmic logicaldisk get drivetype^,volumename^,caption') do (
       echo %%a %%b %%c
    )
    ```

- set: **never put quotation marks around values!**
- log & quit on error: `cmd || echo Failed! && exit` (maybe `&& pause` before exit)
- check if file exists: `dir /b f.txt >nul || echo Missing f.txt! && pause && exit`
- set env vars: `setx NAME VALUE` (`/m` for global)
- enable/disable command extensions (enabled by default!):
    - `cmd /x` / `cmd /y` (run command with extensions enabled: `cmd /x /k mkdir c:\a\b\c` - order matters!)
    - global: `reg add /f "HKCU\Software\Microsoft\Command Processor" /v EnableExtensions /d 1 /t REG_DWORD`
- compress each file with gzip: `for %i in (*.json) do 7z a "%i.gz" "%i"`
    - `%~ni` extracts the name without extension
- download file into variable: `for /f %%i in ('powershell -c "(curl http://2016.eicar.org/download/eicar.com.txt).RawContent"^|findstr X5') do set eicar=%%i`

# PowerShell


## Security


### Change execution policy

- possible values: `Restricted` (interactive only), `AllSigned`, `RemoteSigned`, `Unrestricted`
- query reg key: `reg query "HKLM\Software\Microsoft\PowerShell\1\ShellIds\Microsoft.PowerShell" /v ExecutionPolicy`
- alternatives:
    - set reg key: `reg add "HKLM\Software\Microsoft\PowerShell\1\ShellIds\Microsoft.PowerShell" /v ExecutionPolicy /d "Unrestricted"`
    - per user, via commmand line: `powershell -Command "Set-Executionpolicy -ExecutionPolicy UnRestricted -Force"`
        - for 32 bit apps prefix with `C:\Windows\SysWOW64\cmd.exe /c ...`
        - for the current user only: `-Scope CurrentUser`
    - disable for the current PS session:
        ```
        function Disable-ExecutionPolicy {($ctx = $executioncontext.gettype().getfield("_context","nonpublic,instance").getvalue( $executioncontext)).gettype().getfield("_authorizationManager","nonpublic,instance").setvalue($ctx, (new-object System.Management.Automation.AuthorizationManager "Microsoft.PowerShell"))}
        Disable-ExecutionPolicy
        ./runme.ps1
        ```
- disable for the current command: `powershell.exe -executionpolicy bypass -command "& 'c:\x.ps1'"`
    - or: `powershell -executionpolicy bypass -file .\x.ps1`
    - or (working on 1.0): `powershell -command - < myscript.ps1`
- reference:
    - <https://technet.microsoft.com/en-us/library/ee176961.aspx>
    - <https://blog.netspi.com/15-ways-to-bypass-the-powershell-execution-policy/>


### Add firewall rules

```powershell
New-NetFirewallRule -DisplayName FAKEWMI -Enabled True -Profile Any -Direction Inbound -Program
C:\service\FakeWMIService.exe -Protocol Tcp -LocalPort Any -RemotePort Any -LocalAddress Any -RemoteAddress Any
New-NetFirewallRule -DisplayName FAKEWMI -Enabled True -Profile Any -Direction Outbound -Program
C:\service\FakeWMIService.exe -Protocol Tcp -LocalPort Any -RemotePort Any -LocalAddress Any -RemoteAddress Any
```

- reference: <https://www.exploit-db.com/exploits/41903/>


### Restart service

- `Restart-Service ... -Force`

# Windows JScript

- run in console: `cscript myscript.js`
- default extensions: `js` / `jse`


## Language reference

- data type of a var: `typeof(x)` => string (`"string"`, `"object"`, `"undefined"`, `"number"`, etc.)


### Strings

- length attribute: `"abc".length"`
- typecast to string: `String(x)` or `x.toString()`


### Objects

- iterate attributes / values: `for (var k in obj) { WScript.Echo(String(k) + "=" + String(obj[k])); }`


## Code examples

- convert object to JSON:

    ```JS
    function to_json(o)
    {
      var res = [];
      for (var k in o)
      {
        var v = (o[k].toString() === "[object Object]") ? to_json(o[k]) : ('"' + o[k].toString() + '"');
        res.push('"' + k.toString() + '": ' + v);
      }
      return '{' + res.join(', ') + '}';
    }
    ```

- better to_json:

    ```JS
    function to_json(o)
    {
      if (typeof(o) == "object")
      {
        var is_array = !(String(o) === "[object Object]");
        var a = [];
        for (var k in o)
        {
          a.push(is_array ? to_json(o[k]) : '"' + k + '": ' + to_json(o[k]));
        }
        v = is_array ? ('[' + a.join(', ') + ']') : ('{' + a.join(', ') + '}');
      }
      else
      {
        v = (typeof(o) == 'string') ? ('"' + String(o).replace(/\\/g, '\\\\') + '"') : String(o);
      }
      return v;
    }
    ```

- HTTP POST form:

    ```JS
    function http_post(url, data)
    {
        var oh = new ActiveXObject("Microsoft.XMLHTTP");
        oh.open("POST", url, false);
        oh.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        oh.setRequestHeader("Content-Length", data.length);
        oh.send(data);
        return oh.responseText;
    }
    ```

    - for JSON data use `"application/json; charset=UTF-8"`

- get Windows info:

    ```JS
    var ows = GetObject("winmgmts:{impersonationLevel=impersonate}!//./root/cimv2");
    var oss = ows.ExecQuery("select * from Win32_OperatingSystem");
    for (var e = new Enumerator(oss); !e.atEnd(); e.moveNext())
    {
        os = e.item();
        WScript.Echo("OS: " + os.Name);
        WScript.Echo("OS Language: " + os.OSLanguage);
    }
    ```

- list all files in `c:`:

    ```JS
    var fso = new ActiveXObject("Scripting.FileSystemObject");
    for (var e = new Enumerator(fso.GetFolder("c:\\").files); !e.atEnd(); e.moveNext())
    {
        var file = e.item();
        WScript.echo(file.name);
    }
    ```

- check if folder exists:

    ```JS
    fso = new ActiveXObject("Scripting.FileSystemObject");
    if (fso.FolderExists("c:\\windows")) { ...; }
    ```

- read registry key:

    ```JS
    var o = new ActiveXObject("WScript.Shell");
    WScript.Echo(o.RegRead("HKLM\\System\\CurrentControlSet\\Control\\SystemBootDevice"));
    ```

- messing around with shares (unsuccessfully):

    ```JS

    var lst = ows.ExecQuery("select * from Win32_LogicalShareSecuritySetting");
    for (var e = new Enumerator(lst); !e.atEnd(); e.moveNext())
    {
        v = e.item();
        WScript.Echo("cap: " + v.Caption + "; desc: " + v.Description + "; name: " + v.Name + "; sid: " + v.SettingID);
        var sd = ows.Get("Win32_SecurityDescriptor");
        var method = lst.Methods_.Item("GetSecurityDescriptor");
        var sd = lst.ExecMethod_(method.Name);
        var descriptor = sd.Descriptor;

        // var outParams = fileSecurity.ExecMethod_(method.Name);
        for (var ee = new Enumerator(sd.DACL); !ee.atEnd(); ee.moveNext())
        {
            vv = ee.item();
            WScript.Echo("name: " + vv.Trustee.Name);
        }
    }
    ```

- dumping share access list:

    ```JS
    var lst = ows.ExecQuery("select * from Win32_Share");
    for (var e = new Enumerator(lst); !e.atEnd(); e.moveNext())
    {
        v = e.item();
        var name = v.Name;
        info[name] = {"path":v.Path, "status":v.Status, "title":v.Caption, "access":[]};
    }

    var lst = ows.ExecQuery("select * from Win32_LogicalShareAccess");
    for (var e = new Enumerator(lst); !e.atEnd(); e.moveNext())
    {
        v = e.item();
        var ssec = ows.Get(v.SecuritySetting);
        var tee = ows.Get(v.Trustee);

        info[ssec.Name]["access"].push({"user":tee.AccountName, "domain":tee.ReferencedDomainName,
            "access":v.AccessMask});
        // see Win32_SID.AccessMask @ https://msdn.microsoft.com/en-us/library/aa394450(v=vs.85).aspx for the meaning
        // of the "access" bits
    }
    ```

- sample (not working?) of `GetSecurityDescriptor()`:

    ```JS
    var pathname= "c:\\autoexec.bat";
    var wmiService = GetObject("winmgmts:");
    var fileSecurity = wmiService.Get("Win32_LogicalFileSecuritySetting='" + pathname + "'");

    // Create the method object.
    var method = fileSecurity.Methods_.Item("GetSecurityDescriptor");

    // Retrieve the outParameters object (result of method call).
    var outParams = fileSecurity.ExecMethod_(method.Name);

    // The ReturnValue property is 0 for success.
    if (outParams.ReturnValue == 0) {
      var descriptor = outParams.Descriptor;
      WScript.Echo(descriptor.Owner.Domain + "\\" + descriptor.Owner.Name);
    }
    else {
      WScript.Echo("Couldn't retrieve security descriptor.");
    }
    ```


## Reference

- JScript builtin methods / functions: <https://docs.microsoft.com/en-us/previous-versions/windows/internet-explorer/ie-developer/scripting-articles/c6hac83s%28v%3dvs.84%29>
- ActiveX object: <https://docs.microsoft.com/en-us/previous-versions/windows/internet-explorer/ie-developer/scripting-articles/7sw4ddf8%28v%3dvs.84%29>
- examples:
    - [@informit](https://www.informit.com/articles/article.aspx?p=1187429&seqNum=5<Paste>)
    - [@vbsedit.com](http://www.vbsedit.com/scripts/default.asp)
- share info:
    - Win32_Win32_LogicalShareAccess @ <https://msdn.microsoft.com/en-us/library/aa394186(v=vs.85).aspx>
    - Win32_SID @ <https://msdn.microsoft.com/en-us/library/aa394450(v=vs.85).aspx>

# bash scripting


## Misc

- indirect variable (variable name in another variable): `$!name` (e.g. `a=3; b=a; echo ${!b}`)
- keep history from parallel terminals:

    ```
    export HISTCONTROL=ignoredups:erasedups
    shopt -s histappend
    ```

- bigger history file size:

    ```
    export HISTSIZE=1000000
    export HISTFILESIZE=1000000
    ```
- colors to use in `echo -e` / `printf`:

    ```sh
    CRED='\033[1;31m'
    CGRN='\033[1;32m'
    CCYN='\033[1;36m'
    CRST='\033[0m'
    ```

- file was created in the last 30 min: `if [ -n "find -name $fn -cmin -30" ]; then ...` (`-mmin` for modified)
    - note the `-` in front of 30

- extract text with regex without `grep -o` (not available in e.g. `busybox`): `sed -n 's/.*\(...\).*/\1/p'` or
    `sed -rn 's/.*?(...).*/\1/p'`
    - `-n` = quiet
    - `/p` = print
    - NOTE: sed is ALWAYS greedy!
    - e.g. `echo "abc def ghi"|sed -n "s/.*\(d..\).*/\1/p"`
    - `sed .../I` works more often than `sed .../i`, but not always (to replace `grep -i`)


# Image processing quickref: cv2, numpy, PIL / Pillow, MatPlotLib


## Generic information

- pixel channels: `BGR` for *cv2*, `RGB[A]` for all others
- index order: (x, y) for PIL, (y, x) for *cv2* / *numpy*
- by convention, image variable name: `cimg` (*cv2*), `nimg` (*numpy*), `pimg` (*pil*)
- EXIF: `exif_data = pimg._getexif()`
- size:

    ```python
    h, w, depth = cimg.shape
    # pixelcount = cimg.size
    ```

    - `w, h = pimg.size`


## Imports

- `import cv2`
- `import numpy as np`
- `from PIL import Image, ImageFont, ImageDraw, ImageFilter`
- `from matplotlib import pyplot as plt`


## Image operations

- create:

    ```
    cimg = np.zeros((h, w, depth), np.uint8)
    pimg = Image.new("RGB", (w, h), color)
    ```

- load:

    ```
    cimg = cv2.imread('image.png')
    pimg = Image.open('image.png')
    ```

- save:

    ```
    cv2.imwrite('image.png', cimg)
    pimg.save('image.jpg')
    ```

- convert:
    - PIL to numpy: `nimg = numpy.array(pimg)`
    - PIL to cv2: `cimg = cv2.cvtColor(numpy.array(pimg), cv2.COLOR_RGB2BGR)`
    - make RGB / grayscale:

        ```
        pimg_rgb = pimg.convert('RGB') 
        cimg_gray = cv2.cvtColor(cimg, cv2.COLOR_BGR2GRAY)
        ```

## Pixel access

- `col = cimg[y, x]`
- region access:
    - cv2:

        ```python
        subimg = cimg[y0:y1, x0:x1]
        cimg[y0:y1, x0:x1] = subimg
        ```


## Filters

- `pimg.filter(ImageFilter.SHARPEN)`
- `cv2.blur(cimg, (5, 5))`


## Geometric processing

- resize:

    ```
    cimg2 = cv2.resize(cimg, (0,0), fx=0.5, fy=0.5)                                 # proportional
    cimg2 = cv2.resize(cimg, (width, height), interpolation=cv2.INTER_AREA)      # fixed size result
    pimg2 = pimg.resize((w, h), Image.ANTIALIAS)
    ```


## Channels

```python
b, g, r = cv2.split(img)
cimg = cv2.merge((b, g, r))
```

- `r, g, b = pimg.split()`


## Show image

- cv2:
    
    ```python
    # cv2.namedWindow('image', cv2.WINDOW_NORMAL)
    cv2.imshow('Window Title', img)
    key = cv2.waitKey(0)
    cv2.destroyAllWindows()
    ```

- pil:
    `pimg.show()`

    
## Misc

- add border:
    - `cimg2 = cv2.copyMakeBorder(cimg, top, bottom, left, right, cv2.BORDER_CONSTANT, value=(255, 0, 0))`

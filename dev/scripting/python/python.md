# Python


## Debugging

- print current function and local variables: `print('%s(): %s' % (sys._getframe().f_code.co_name, locals()))`


## Networking

- custom behavior using one or more handlers:
    - validate the server using a certificate:

        ```python
        import ssl, urllib.request

        context = ssl.SSLContext(ssl.PROTOCOL_TLSv1_2)
        context.verify_mode = ssl.CERT_REQUIRED
        context.check_hostname = True
        context.load_verify_locations('/path/to/cert.crt') # .pem also works
        hh = urllib.request.HTTPSHandler(context=context)
        o = urllib.request.build_opener(hh)
        r = o.open(url)
        data = r.read()
        return data
        ```

    - use BASIC AUTH:

        ```python
        ah = urllib.request.HTTPBasicAuthHandler()
        ah.add_password(realm='foo', uri=url, user=user, passwd=password)
        o = urllib.request.build_opener(ah, hh)
        # ... (see above)
        ```


## Libraries

- [pymunk](http://www.pymunk.org/en/latest/pymunk.html)
    - "Pymunk is a easy-to-use pythonic 2d physics library that can be used whenever you need 2d rigid body physics from
        Python."


## Windows

- give focus to window:

    ```Python
    from ctypes import windll, WINFUNCTYPE
    from ctypes.wintypes import HWND, BOOL, UINT, DWORD, PDWORD

    user32 = windll.user32
    ShowWindow = WINFUNCTYPE(BOOL, HWND, DWORD)(("ShowWindow", user32))
    SetWindowPos = WINFUNCTYPE(BOOL, HWND, HWND, DWORD, DWORD, DWORD, DWORD, UINT)(("SetWindowPos", user32))
    mouse_event = WINFUNCTYPE(None, DWORD, DWORD, DWORD, DWORD, PDWORD)(("SetWindowPos", user32))

    HWND_TOPMOST = HWND(-1)
    HWND_NOTOPMOST = HWND(-2)
    SW_RESTORE = 9
    SWP_NOSIZE = 0x0001
    SWP_NOMOVE = 0x0002
    SWP_SHOWWINDOW = 0x0040
    MOUSEEVENTF_LEFTDOWN = 0x0002
    MOUSEEVENTF_LEFTUP = 0x0004

    ShowWindow(hwnd, SW_RESTORE)
    SetWindowPos(hwnd, HWND_NOTOPMOST, 0, 0, 0, 0, SWP_NOMOVE + SWP_NOSIZE)
    SetWindowPos(hwnd, HWND_TOPMOST, 0, 0, 0, 0, SWP_NOMOVE + SWP_NOSIZE)
    SetWindowPos(hwnd, HWND_NOTOPMOST, 0, 0, 0, 0, SWP_SHOWWINDOW + SWP_NOMOVE + SWP_NOSIZE)
    mouse_event(MOUSEEVENTF_LEFTDOWN, 0, 0, 0, None)
    mouse_event(MOUSEEVENTF_LEFTUP, 0, 0, 0, None)
    ```


## Misc

- get function info:
    - parameter names: `fun.__code__.co_varnames` (tuple of strings)
    - count: `.__code__.co_argcount`
    - default param values: `.__defaults__`

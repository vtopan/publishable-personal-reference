# networkx

- network modelling tool
- [documentation](https://networkx.github.io/documentation/latest/)
- use in `jupyter`: `import networkx as nx`

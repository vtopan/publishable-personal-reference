# Jupyter / IPython

- install: `sudo -H pip3 install --upgrade jupyter`
    - add `pyside` for Qt bindings (needed by Vim integration)
- run: `jupyter notebook` => <http://localhost:8888/tree> will show the current user's home folder; open an `.ipynb`


# Python PySide (Qt wrapper)

- alternative to PyQt


## Snippets

- modules:
    - `PySide.QtCore`: Qt, QObject, Slot, QEvent
    - `PySide.QtGui`: QApplication, QHBoxLayout, QVBoxLayout, QWidget, QLineEdit, QLabel, QKeySequence, QAction,
        QMainWindow, QPushButton
    - `PySide.QtWebKit`: QWebView, QWebPage
- button: `but = QPushButton('&Find', self, clicked=self.find)`
    - can connect to action separately: `but.clicked.connect(self.find)`
- get / set widget text: `.text()` ? / `.setText(...)`
- tooltip: `item.setToolTip('Hello!')`
- default (placeholder) text in edits: `lineedit.setPlaceholderText("search text (Alt+F)")`
- event on text change: `lineedit.textChanged.connect(self.text_changed)` + `def text_changed(self, text): ...`
- layouts:
    - `self.layout = QVBoxLayout()`
        - `self.layout.addWidget(self.button)` (next param can be integer proportion, e.g. `addWidget(but, 5)`)
        - `self.layout.addLayout(self.hlayout)`
    - `QHBoxLayout`, `QBoxLayout` (include x+y coords in `add*()` calls)
- HTML viewer: `self.web = QWebView()`
    - content: `self.web.setHtml('...')`
    - interact with JS in the page:
        - expose Python object to JS: `self.web.page().mainFrame().addToJavaScriptWindowObject('nameInJs', pyobj)`
        - run JS in page context: `self.web.page().mainFrame().documentElement().evaluateJavaScript("...")`
        - click JS link by ID:
            - get anchor tag proxy: `a = self.results.page().mainFrame().documentElement().findFirst('a[id=...]')`
            - click: `a.evaluateJavaScript("this.click()")`
    - find text: `self.web.findText(text)`
        - wrap: `.findText(text, QWebPage.FindWrapsAroundDocument)`
        - highlihht all: `.findText(text, QWebPage.HighlightAllOccurrences)`
- focus widget on shortcut:
    `self.addAction(QAction("Find", self, shortcut=QKeySequence("Alt+F"), triggered=widget.setFocus))`
- set widget colors: `widget.setStyleSheet('background-color:#EDB;')`
- make window:
    - borderless: `win.setWindowFlags(self.windowFlags() | Qt.WindowStaysOnTopHint)`
    - frameless: `win.setWindowFlags(self.windowFlags() | Qt.FramelessWindowHint)`
    - use `& ~Qt...` to disable flag (and `.show()` after removing stay on top flag), `^` to toggle

- drag by clicking anywhere on window:

    ```Python
    def mousePressEvent(self, evt):
        self.drag_base_pos = evt.pos()

    def mouseMoveEvent(self, evt):
        if evt.buttons() & Qt.LeftButton:
            diff = evt.pos() - self.drag_base_pos
            self.move(self.pos() + diff)
    ```
  
- toggle visibility of an item: `x.setVisible(not x.isVisible()); parent.adjustSize()`
    - adjust vertical size: `self.resize(width(), minimumSizeHint().height())`
- load font from TTF/OTF file:

    ```Python
    db = QtGui.QFontDatabase()
    font_id = db.addApplicationFont(font_file)
    name = db.applicationFontFamilies(font_id)[0]
    font = QtGui.QFont(name)
    ```
    
- get current QApplication instance: `PySide2.QtCore.QCoreApplication.instance()`
- center(ish) window:

    ```Python
    rect = self.frameGeometry()
    pos = QtWidgets.QDesktopWidget().availableGeometry().center()
    rect.moveCenter(pos)
    self.move(pos.x(), pos.y())
    ```

- set vertical scrollbar width via CSS: `QScrollBar:vertical {width: 10px;}`
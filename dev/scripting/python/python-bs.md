# BeautifulSoup


## Misc

- find:
    - comments: `.find_all(text=lambda text:isinstance(text, bs4.Comment))`
    - by class regex: `.find_all(attrs={'class':re.compile('navbar|noprint')})`
- remove:
    - node: `.decompose()` (comments only have `.extract()`, which also returns the node)
    - node contents: `.clear()`
    - node, but keep contents: `.unwrap()`
    - and replace: `.replace_with(...)`
- insert: `.append(BeautifulSoup('<b>hello!</b>'))`
- pretty print: `.prettify()`
    - encode special Unicode chars as HTML entities: `.prettify(formatter='html')`


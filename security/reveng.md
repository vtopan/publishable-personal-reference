# Reverse engineering


## ELF

- get info from binary:
    - tools: `file`, `eu-readelf` (`elfutils`), `ldd`, `readelf`, `rabin2` (`radare2`), `objdump`,
        `dumpelf` / `scanelf` (`pax-utils`)
    - general: `readelf -a` (full), `rabin2 -I` (general info), `objdump -x` (all headers)
    - imports: `rabin2 -i`, `objdump -tT`, `readelf -s`
        - check dependencies: `ldd`

- reference:
    - <https://linux-audit.com/elf-binaries-on-linux-understanding-and-analysis/>

# Network Security

- packet / traffic analysis tools:

    - `NetworkMiner`:

        ```
        # install mono
        sudo apt-get install -i libmono-system-windows-forms4.0-cil libmono-system-web4.0-cil libmono-system-net4.0-cil libmono-system-runtime-serialization4.0-cil
        # install NM to /opt
        wget www.netresec.com/?download=NetworkMiner -O /tmp/nm.zip
        sudo unzip /tmp/nm.zip -d /opt/
        cd /opt/NetworkMiner*
        sudo chmod -R go+w AssembledFiles Captures
        ```

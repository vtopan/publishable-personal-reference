# OpenSSL


## Reference

- <https://www.sslshopper.com/article-most-common-openssl-commands.html>
- <https://www.digitalocean.com/community/tutorials/openssl-essentials-working-with-ssl-certificates-private-keys-and-csrs>


## Subcomands & parameters

- `-des`, `-des3`: private key encryption mode
    - `-nodes`: don't encrypt private key
- `-out`: output file
- `-days N`: certificate validity (default 30)
- `-subj "/C=US/ST=Denial/L=Springfield/O=Dis/CN=www.example.com"`: non-interactive cert subject
- `-passin ...` / `-passout ...`: input / output file password
    - the argument can be `pass:...` (the actual password), `env:VAR` (env. var. `VAR`), `file:...` (from file),
      `fd:...` (file descriptor), `stdin` (default)
- prefix command with `OPENSSL_CONF= ` to override default distro OpenSSL configuration
- subcommands:
  - `genrsa [nbits]`: generate private key
  - `req`: generate CSR (optionally the private key as well with `-newkey`)
    - `-keyout ...`: private key output file (with `-newkey`)
    - `-key ...`: private key (existing)
    - `-x509`: generate self-signed cert instead of a CSR
  - `x509`: generate/convert certificate
  - `s_client -connect <host>:<port>`: connect through an SSL tunnel to a host
- list available eliptic curves: `openssl ecparam -list_curves`
- display certificate: `openssl x509 -noout -text -in cert.crt`


## Examples

- generate self-signed certificate + private key (non-interactive):

    ```
    openssl req -new -newkey rsa:4096 -nodes -x509 -days 365 \
        -subj "/C=US/ST=Denial/L=Springfield/O=Dis/CN=www.example.com" \
        -keyout site.key -out site.cert
    ```
- remove password from key: `openssl rsa -in cert.key -out nopass.key`
- convert PEM to DER: `openssl x509 -outform der -in cert.pem -out cert.der`
- convert CRT to PEM: `openssl x509 -in mycert.crt -out mycert.pem -outform PEM`
- validate cert of URL: `openssl s_client -connect <URL>:443 -showcerts`
